import React, { Component } from 'react'
import "./admin.css"
import AdminTwo from '../components/admin/admin'
// This Page Should Include: {
//     1. Login In Section 
//     2. Table of All Pending Hardships
//     3. Finacial Data 
// }


class Admin extends Component {
  state={
    auth:false,
    code: ""
  };
  handleCode(e){
    this.setState({
        code: e.target.value
    });
      if (this.state.code==="gatewayGiving"){
        this.setState({
            auth: true
        })
      }
      console.log("code: ", this.state.code)
  }
  render() {
    if (this.state.auth){
        return (
            <div>
                <AdminTwo/>
            </div>
        )
    }else {
      return(
          <div class="hideMe">
              <h1>Enter Passcode</h1>
            <input
            type="text"
            onChange={(e)=>this.handleCode(e)}
            />
          </div>
      )
    }

  }
}

export default Admin