import React, { Component } from "react";
import UserMenu from "../components/User/UserMenu/UserMenu";
import Sidebar from "../components/User/Sidebar/Sidebar";
import MyCampaign from "../components/User/Account/MyCampaign";
import CreateCampaignForm from '../components/User/Account/createCampaignForm2'
import Hardships from '../components/Hardships/Hardship'
import Cart from '../components/Cart/Cart'
import ContactUs from '../components/ContactUs/contactUs2'
import "../components/User/Sidebar/Sidebar.css";
import { connect } from "react-redux";
import Cookies from "js-cookie";
import './User.css'
import { menuFetchData } from "../store/actions/actionCreators2";

// This should have access to a users pending and approved hardships and a way to track how much they have recieved.

class User extends Component {
  componentDidMount() {
    this.props.getUserData2(Cookies.get("UaToken"));
  }
  render() {
    if (this.props.page === "myCampaign") {
      return (
        <div className="container-fluid">
          <UserMenu />
          <div className="row">
            <div id="sbg" className="col col-lg-3">
              <Sidebar id="Sidebar"/>
            </div>
            <div id="userMainSection"className="col col-lg-9">
              <MyCampaign />
            </div>
          </div>
        </div>
      );
    } else if (this.props.page === "createCampaign") {
      return (
        <div className="container-fluid">
          <UserMenu />
          <div className="row">
            <div id="sbg" className="col col-lg-3">
              <Sidebar />
            </div>
            <div className="col col-lg-9">
              <CreateCampaignForm/>
            </div>
          </div>
        </div>
      );
    } else if (this.props.page === "promote") {
      return (
        <div className="container-fluid">
          <UserMenu />
          <div className="row">
            <div id="sbg" className="col col-lg-3">
              <Sidebar />
            </div>
            <div className="col col-lg-9">
              <h1>Promote Component</h1>
            </div>
          </div>
        </div>
      );
    } else if (this.props.page === "donate") {
      return (
        <div className="container-fluid">
          <UserMenu />
          <div className="row">
            <div id="sbg" className="col col-lg-3">
              <Sidebar />
            </div>
            <div className="col col-lg-9">
              {/* <h1>Donate Component</h1> */}
              <div className="row">
                <div className="col col-lg-7">
                    <Hardships/>
                  </div>
                  <div className="col col-lg-5">
                    <Cart/>
                  </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else if (this.props.page === "contactUs") {
      return (
        <div className="container-fluid">
          <UserMenu />
          <div className="row">
            <div id="sbg" className="col col-lg-3">
              <Sidebar />
            </div>
            <div className="col col-lg-9">
              <ContactUs/>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="container-fluid">
          <UserMenu />
          <div className="row">
            <div id="sbg" className="col col-lg-3">
              <Sidebar />
            </div>
            <div className="col col-lg-9">
              <MyCampaign />
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
    page: state.sidebar.page
  };
};

const mapDispatchToState = dispatch => {
  return {
    getUserData2: token => dispatch(menuFetchData(token))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToState
)(User);
