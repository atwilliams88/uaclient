import React, { Component } from 'react'
import Menu from '../components/Menu/Menu'
import Hardships from '../components/Hardships/Hardship'
import Cart from '../components/Cart/Cart'
import './index.css'
import './Need.css'
import '../components/Menu/Menu.css'
import Help from '../components/helpButton/help'



class Needs extends Component {

componentDidMount () {
    // console.log(this.getWindow())
};
getWindow () {
    return window.frames.innerWidth
}
  render() {
    return (
      <div>
        <div 
        // className="jumbotron jumbotron-fluid"
        id="needHeader"
        >
        <Menu/>
          <h1 id="NeedCta">Browse Hardships</h1>
          <h2 id="headerH2">People around the world are raising money for what they are passionate about.</h2>
            <br/>
            <div className="row">
                <a className="m20 btn btn-primary" href="/needs/5c05f2a8298d8a17a916caf4">Donate To Utility Assist</a>
                <div className="m20 moHelp "><Help className={"moHelp"}/></div>
            </div>

        </div>
        <div className="row">
          <div  className="col col-lg-6 order-md-first">
            <Hardships/>
          </div>
          <div className="col col-lg-6 order-first">
            <Cart/>
          </div>
        </div>
      </div>
    )
  }
}

export default Needs