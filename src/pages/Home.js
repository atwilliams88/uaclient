import React, { Component } from "react";
import Header from "../components/Header/Header";
import Newsletter from "../components/Home/Newsletter";
import FAQ from '../components/Faq/FAQ'
import Donate from '../components/Donate/Donate'
import Register from '../components/Register/Register'
import './index.css'
import '../img/Screen Shot 2018-07-24 at 9.25.47 PM.png'
class Home extends Component {
  state = {
    page: "default"
  };
  render() {
    if (this.state.page ==="default")
    return (
      <div>
        <Header/>
        <div className="row justify-content-center">
          <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12 mainSpacing">
          <Donate/>
          </div>
          <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12 mainSpacing">
          <Register/>
          </div>
          <div className="col-lg-6 col-md-12 col-sm-12  col-xs-12 mainSpacing">
          <Newsletter/>
          </div>
          <div className="col-lg-6 col-md-12  col-sm-12 col-xs-12 mainSpacing">
          <FAQ/>
          </div>
        </div>
        <footer className="container py-5">
          <div className="row">
            <div className="col-12 col-md">
              <small className="d-block mb-3 text-muted">
                &copy; Established 2019
              </small>
            </div>
            <div className="col-6 col-md">
              <h5>More Info</h5>
              <ul className="list-unstyled text-small">
                <li>
                  <a className="text-muted" onClick={()=>this.setState({page:"aboutUs"})}>
                    About Us
                  </a>
                </li>
                <li>
                  <a className="text-muted" onClick={()=>this.setState({page:"howItWorks"})}>
                    How It Works
                  </a>
                </li>
              </ul>
            </div>

            <div className="col-6 col-md">
              <button
              className="btn btn-outline-success"
              onClick={()=>this.setState({page:"default"})}
              >Home
              </button>
            </div>
          </div>
        </footer>
      </div>
    );
    //About Us Page
    if (this.state.page ==="aboutUs") {
     return (
          <div>
              <Header/>
              <div className="row justify-content-start m-5 p-5 homeBox">
                 <h1>About Us</h1>
                  <p>
                      We are a non-profit organization that is created to stabilize households with direct donations for utility
                      assistance. No matter your ethnicity, gender, culture, or economic status at some point in our lives we
                      all find ourselves in need of help. This website is designed to allow consumers to receive the help they
                      need and donors to be in total control of where their money is going.
                      Our vision is to become a leader in stabilizing communities through utility assistance. Help your
                      community by helping others in need.
                  </p>
                  <button
                      className="btn btn-outline-success"
                      onClick={()=>this.setState({page:"default"})}
                  >Home
                  </button>
              </div>
              <footer className="container py-5">
                  <div className="row">
                      <div className="col-12 col-md">
                          <small className="d-block mb-3 text-muted">
                              &copy; Established 2019
                          </small>
                      </div>
                      <div className="col-6 col-md">
                          <h5>More Info</h5>
                          <ul className="list-unstyled text-small">
                              <li>
                                  <a className="text-muted" onClick={()=>this.setState({page:"aboutUs"})}>
                                      About Us
                                  </a>
                              </li>
                              <li>
                                  <a className="text-muted" onClick={()=>this.setState({page:"howItWorks"})}>
                                      How It Works
                                  </a>
                              </li>
                          </ul>
                      </div>

                      <div className="col-6 col-md">
                          <button
                              className="btn btn-outline-success"
                              onClick={()=>this.setState({page:"default"})}
                          >Home
                          </button>
                      </div>
                  </div>
              </footer>
          </div>

      );
    }
    //How it works page
      if (this.state.page ==="howItWorks") {
          return (
              <div>
                  <Header/>
                  <div className="row justify-content-start m-5 p-5 homeBox">
                      <h1>How It Works</h1>
                      <p>
                          First, a consumer would need to register to have their hardship publicly displayed. You will be assigned a computer generated hardship identification number (ID #). The hardship ID # will be the only information that identifies your need for help.  All personal identification information will NOT be on display. UtilityAssist will remove any account from public display if it has personal related information. <a href="/needs">Click here</a> to review examples of hardships.
                      </p>
                      <p>
                          It will be your responsibility to direct donors to utility-assist.org to donate towards your utility accounts using your hardship ID #. That means the CONSUMER will have to solicit and market their needs to the public. This can be done by advertising in the St. Louis American, church bulletins, Facebook, Instagram, etc. Your advertisement would
                          direct donors to utility-assist.org website to donate towards your utility accounts specific needs.
                      </p>
                      When someone donates to an account, the consumer will receive an email notification. All communication will be done through the required email address the consumer provides in their application. If the email address provided by the consumer is invalid, we will attempt to contact you through alternative resources that were provided on the application. If all attempts fail, the consumer account will be removed from public display.
                      <p>
                          The donor will inform utility-assist where to apply the funds. When a donor selects an account, their information will not be disclosed to the consumer or public. Tax exempt letters will be sent to the donor’s email address after all payment transaction has been completed.
                      </p>
                      In the event the account the donor selects has been paid in full or has been removed from the site, the donor will receive an email from a utility-assist representative for further instructions for processing their payment. The donor will ALWAYS be in control of their funds.
                      <p>
                          All payment methods are conducted by stripe payment website. There is a fee for each transaction that will be deducted from the donated amount. For example, if you choose to donate $100 towards an account. The consumer would receive $90 donation towards their account. There is a 10% processing fee. Three percent (3%) goes toward the credit card transaction fee and the 7% fee goes towards the cost of maintaining the website. All fees are subject to change according to the financial institution conducting the payment transactions. Utility-assist is a hosting website. Utility-assist is not responsible for the costs associated with any financial institution conducting the payment transactions.
                      </p>
                      <p className="full">
                          <bold>
                              <h3>Contact Us</h3><br/>
                              Can we create an email address for the consumer to notify us for any questions?<br/>
                              Email address:  customerservice@utility-assist.org
                          </bold>
                      </p>
                      <button
                          className="btn btn-outline-success"
                          onClick={()=>this.setState({page:"default"})}
                      >Home
                      </button>
                  </div>
                  <footer className="container py-5">
                      <div className="row">
                          <div className="col-12 col-md">
                              <small className="d-block mb-3 text-muted">
                                  &copy; Established 2019
                                    <img src="../img/Screen%20Shot%202018-07-24%20at%209.25.47%20PM.png"/>
                              </small>
                          </div>
                          <div className="col-6 col-md">
                              <h5>More Info</h5>
                              <ul className="list-unstyled text-small">
                                  <li>
                                      <a className="text-muted" onClick={()=>this.setState({page:"aboutUs"})}>
                                          About Us
                                      </a>
                                  </li>
                                  <li>
                                      <a className="text-muted" onClick={()=>this.setState({page:"howItWorks"})}>
                                          How It Works
                                      </a>
                                  </li>
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Team feature*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Stuff for developers*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Another one*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Last time*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                              </ul>
                          {/*</div>*/}
                          {/*<div className="col-6 col-md">*/}
                              {/*<h5>Resources</h5>*/}
                              {/*<ul className="list-unstyled text-small">*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Resource*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Resource name*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Another resource*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Final resource*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                              {/*</ul>*/}
                          {/*</div>*/}
                          {/*<div className="col-6 col-md">*/}
                              {/*<h5>Resources</h5>*/}
                              {/*<ul className="list-unstyled text-small">*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Business*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Education*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Government*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                                  {/*<li>*/}
                                      {/*<a className="text-muted">*/}
                                          {/*Gaming*/}
                                      {/*</a>*/}
                                  {/*</li>*/}
                              {/*</ul>*/}
                          </div>
                          <div className="col-6 col-md">
                              <button
                                  className="btn btn-outline-success"
                                  onClick={()=>this.setState({page:"default"})}
                              >Home
                              </button>
                          </div>
                      </div>
                  </footer>
              </div>
          );
      }
  }
}

export default Home;
