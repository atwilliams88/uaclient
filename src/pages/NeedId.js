import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionTypes from "../store/actions/actions";
import axios from "axios";
import Cart from "../components/Cart/Cart";
import Menu from "../components/Menu/Menu";
import "./needId.css"

class NeedId extends Component {
  state = {
    hardship: {},
    notFound: false,
    customAmount: 0,
    transactionGroupId: ""
  };

  componentDidMount() {
    this.getSomeNeed();
    this.generateId();
  }
  generateId() {
    var transactionGroupId = Math.floor(Math.random() * 10000000000000000000);
    this.setState({
      transactionGroupId: transactionGroupId
    });
  }

  getSomeNeed() {
    axios({
      method: "GET",
      url: "/api/hardship/id/" + this.props.match.params.id
    })
      .then(response => {
        // console.log(response);
        if (response.status === 200) {
          this.setState({
            hardship: response.data.hardship[0]
          });
          // console.log(this.state.hardship.Owner._id);
        } else {
          this.setState({
            notFound: true
          });
        }
      })
      .catch(error => {
        if (error) {
          this.setState({
            notFound: true
          });
        }
      });
  }
  // render() {
  //   return (
  //     <div>
  //       Test
  //     </div>
  //   )

  render() {
    if (!this.state.hardship) {
      return <div>Please wait....</div>;
    } else if (this.state.notFound === true) {
      return (
        <div
          // className="jumbotron jumbotron-fluid"
          id="needHeader"
        >
          <Menu />
          <h1 id="NeedCta">Yikes! We Didnt find it</h1>
          <h2>
            Try again, remember our Need Numbers are 24 characters long... it
            may be best to use copy and paste
          </h2>
        </div>
      );
    } else {
      // console.log(this.state.hardship);
      return (
        <div>
          <div
            // className="jumbotron jumbotron-fluid"
            id="needHeader"
          >
            <Menu />
            <h1 id="NeedCta">You Found It!</h1>
            <h2 class="special">Help someone special with a hardship they are facing.</h2>
            <br/>
          </div>
          <div className="row">
            <div className="col">
              <h1 className="ml-2">Hardships </h1>
              <div id="HardshipComponent" className="card m-2 p-2 bg-light">
                <h5 className="mb-3">
                  {"Need Number: " + this.state.hardship._id}
                </h5>
                <div className="row">
                  <div className="col col-lg-8 mr-4">
                    <h5 className="font-weight-light">
                      ${this.state.hardship.MoneyRasied} of $
                      {this.state.hardship.Balance} goal
                    </h5>
                    <div className="progress" style={{ width: "100%" }}>
                      <div
                        className="progress-bar"
                        style={{
                          width:
                            (this.state.hardship.MoneyRasied /
                              this.state.hardship.Balance) *
                              100 +
                            "%"
                        }}
                        role="progressbar"
                        aria-valuenow={this.state.hardship.MoneyRasied}
                        aria-valuemin="0"
                        aria-valuemax={this.state.hardship.Balance}
                      >
                        {(
                          (this.state.hardship.MoneyRasied /
                            this.state.hardship.Balance) *
                          100
                        ).toFixed(2) + "%"}
                      </div>
                    </div>
                  </div>
                  <div className="col">
                    <div
                      className="btn-group btn-group-toggle"
                      data-toggle="buttons"
                    >
                      <button
                        className="btn btn-outline-success"
                        onClick={donation =>
                          this.props.addFiveToCart(
                            this.state.hardship,
                            5,
                            this.state.transactionGroupId
                          )
                        }
                      >
                        $5
                      </button>
                      <button
                        className="btn btn-outline-success"
                        onClick={donation =>
                          this.props.addTenToCart(
                            this.state.hardship,
                            10,
                            this.state.transactionGroupId
                          )
                        }
                      >
                        $10
                      </button>
                      <button
                        className="btn btn-outline-success"
                        onClick={donation =>
                          this.props.addTwentyToCart(
                            this.state.hardship,
                            20,
                            this.state.transactionGroupId
                          )
                        }
                      >
                        $20
                      </button>
                    </div>
                  </div>
                </div>
                <p className="font-weight-light">
                  {this.state.hardship.Provider}
                </p>
                <p>{this.state.hardship.Description}</p>
                <div className="row text-align-center">
                  <div className="col">
                    {/*<form id="donationInputForm">*/}
                      <input
                        id="customAmount"
                        type="number"
                        onChange={e => {
                          this.setState({ customAmount: e.target.value });
                        }}
                        placeholder="Or Enter Custom $ Amount"
                      />
                  </div>
                  <div className="col">
                      <submit
                        id="addCustomDonationButton"
                        onClick={(donation, amount) =>
                          this.props.addCustomToCart(
                            this.state.hardship,
                            this.state.customAmount,
                            this.state.transactionGroupId
                          )
                        }
                      >
                        Add Custom Amount
                      </submit>
                    {/*</form>*/}
                  </div>
                </div>
              </div>
            </div>
            <div class="col">
              <Cart />
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    addFiveToCart: (hardship, donation, transactionGroupId) =>
      dispatch({
        type: actionTypes.ADD_FIVE_TO_CART,
        payload: {
          hardship,
          donation,
          transactionGroupId
        }
      }),
    addTenToCart: (hardship, donation, transactionGroupId) =>
      dispatch({
        type: actionTypes.ADD_TEN_TO_CART,
        payload: {
          hardship,
          donation,
          transactionGroupId
        }
      }),
    addTwentyToCart: (hardship, donation, transactionGroupId) =>
      dispatch({
        type: actionTypes.ADD_TWENTY_TO_CART,
        payload: {
          hardship,
          donation,
          transactionGroupId
        }
      }),
    addCustomToCart: (hardship, donation, transactionGroupId) =>
      dispatch({
        type: actionTypes.ADD_CUSTOM_TO_CART,
        payload: {
          hardship,
          donation,
          transactionGroupId
        }
      })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NeedId);
