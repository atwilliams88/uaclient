import * as actionTypes from '../actions/actions'

const initalStateSidebar = {
    page: ""
}

const sidebarReducer = (state = initalStateSidebar, action) => {
    switch (action.type) {
        case actionTypes.VIEW_MY_CAMPAIGN:
        return {
            ...state,
            page: "myCampaign"
        }
        case actionTypes.VIEW_CREATE_CAMPAIGN:
        return {
            ...state,
            page: "createCampaign"
        }
        case actionTypes.VIEW_PROMOTE:
        return {
            ...state,
            page: "promote"
        }
        case actionTypes.VIEW_DONATE:
        return {
            ...state,
            page: "donate"
        }
        case actionTypes.VIEW_CONTACT_US:
        return {
            ...state,
            page: "contactUs"
        }
        case actionTypes.VIEW_PENDING_HARDSHIPS:
        return {
            ...state,
            page: "pendingHardships"
        }
        case actionTypes.VIEW_APPROVED_HARDSHIPS:
        return {
            ...state,
            page: "approvedHardships"
        }
        case actionTypes.VIEW_DENIED_HARDSHIPS:
        return {
            ...state,
            page: "deniedHardships"
        }
        case actionTypes.VIEW_DONOR_LIST:
        return {
            ...state,
            page: "donorList"
        }
        case actionTypes.VIEW_DONATIONS:
        return {
            ...state,
            page: "Donations"
        }
        case actionTypes.VIEW_STATS:
        return {
            ...state,
            page: "stats"
        }

        default:
        return {
            ...state
        }
    }
}

export default sidebarReducer