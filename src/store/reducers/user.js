import * as actionType from '../actions/actions'

var initalState = {
    isLoggedIn: false,
    showSignUpForm: false,
    showLoginForm: false,
    errorMessage: '',
    firstName: '',
    lastName: '',
    email: '',
    phone:'',
    optInToMarketing: '',
    hardships: '',
    userId: ''
}
var userReducer =  (state = initalState, action)=>{
    switch (action.type) {
        case actionType.SIGNUPSUBMISSION: 
            return {
                ...state,
                showSignUpForm: false,
                isLoggedIn: false
            }
        case actionType.SIGNINSUBMISSION:
            return {
                ...state,
                email: action.email,
                isLoggedIn: true,
                showLoginForm: false
            };
        case actionType.MENU_FETCH_DATA:
            console.log("menuFetch: ", action);
            return {
                ...state,
                isLoggedIn: true,
                firstName: action.firstName,
                lastName: action.lastName,
                email: action.email,
                phone: action.phone,
                hardships: action.hardships,
                userId: action.userId,
                address: action.address
            };
        case actionType.SIGNINSUBMISSION_FAIL:
            return {
                ...state,
                errorMessage: 'Please Try Again'
            }
            case actionType.SIGNUP_SUBMISSION_ERROR:
            return {
                ...state,
                errorMessage: 'Email and Phone Must Be Unique'
            }
        case actionType.SENDUSERTOSTATE:
            return {
                ...state,
                isLoggedIn: true,
                showSignUpForm: false,
                firstName: action.firstName,
                lastName: action.lastName,
                phone: action.phone,
                hardships: action.hardships
            }
        case actionType.OPEN_LOGIN:
            return{
                ...state,
                showLoginForm: true,
                showSignUpForm: false
            }
        case actionType.OPEN_CREATE_ACCOUNT: 
            return {
                ...state,
                showSignUpForm: true,
                showLoginForm: false
            }
        default: 
            return {
                ...state
            }
    }
}

export default userReducer