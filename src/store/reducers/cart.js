import * as actionTypes from '../actions/actions'

const initalStates = {
    selectedDonation: [],
    total:0,
    tip:0,
    transactionGroupId: 0,
    basicAmount: 0
};

const cartReducer = (state = initalStates, action) => {
    switch (action.type) {
        case actionTypes.ADD_FIVE_TO_CART: 
        console.log(action.payload.i);
        return {
            ...state,
            selectedDonation: state.selectedDonation.concat({
                hardship: action.payload.hardship,
                donation: action.payload.donation,
                index: action.payload.i,
                filterId: Math.floor(Math.random() * 10000),
            }),
            total: state.total + action.payload.donation,
            transactionGroupId: action.payload.transactionGroupId,

        }
        case actionTypes.ADD_TEN_TO_CART: 
        return {
            ...state,
            selectedDonation: state.selectedDonation.concat({
                hardship: action.payload.hardship,
                donation: action.payload.donation,
                index: action.payload.i,
                filterId: Math.floor(Math.random() * 10000)
            }),
            total: state.total + action.payload.donation,
            transactionGroupId: action.payload.transactionGroupId

        }
        case actionTypes.ADD_TWENTY_TO_CART: 
        return {
            ...state,
            selectedDonation: state.selectedDonation.concat({
                hardship: action.payload.hardship,
                donation: action.payload.donation,
                index: action.payload.i,
                filterId: Math.floor(Math.random() * 10000)
            }),
            total: state.total + action.payload.donation,
            transactionGroupId: action.payload.transactionGroupId

        }
        case actionTypes.ADD_CUSTOM_TO_CART:
        var num = parseInt(action.payload.donation);
        return {
            ...state,
            selectedDonation: state.selectedDonation.concat({
                hardship: action.payload.hardship,
                donation: action.payload.donation,
                index: action.payload.i,
                filterId: Math.floor(Math.random() * 10000)
            }),
            total: (state.total + num),
            transactionGroupId: action.payload.transactionGroupId
        };
        case actionTypes.REMOVE_DONATION:
        // console.log(action.payload.donationId)
        // console.log(action.payload.amount)
        // console.log(state.selectedDonation[0].hardship._id)
        console.log(action.payload.filter);
        var filteredDonation = state.selectedDonation.filter(donation => donation.filterId!==action.payload.filter) 
        return {
            ...state,
            selectedDonation: filteredDonation,
            total: state.total - action.payload.amount
        };
        //todo fix this
        case 'ADD_TIP': {
            return {
                ...state,
                tip: action.payload.tip,
                stripeAmount: ((action.payload.tip + state.total) *100),
                basicAmount:action.payload.tip+state.total
            }
        }
        default:
        return {
            ...state
        }
    }
}

export default cartReducer