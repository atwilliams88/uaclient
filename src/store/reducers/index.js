import * as actionType from '../actions/actions'

var initalState = {
    firstName:'Alex',
    lastName:'Williams',
}
var testReducer =  (state = initalState, action)=>{
    if(action.type ===actionType.NEWNAME){
        return {
            firstName: action.payload,
            lastName: 'Blair'
        }
    }
    return state
}

export default testReducer