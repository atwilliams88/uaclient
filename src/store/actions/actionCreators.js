import * as actionTypes from './actions'
import axios from 'axios'


////////// For User Sign Up
export const signUpSubmissionSave = ()=>{
    return {
        type: actionTypes.SIGNUPSUBMISSION,
        hideSignUpForm: true,
    }
}

export const signUpSubmissionError = ()=>{
    return {
        type: actionTypes.SIGNUP_SUBMISSION_ERROR,
        errorMessage: 'Please Try Again - email and phone number must be unique'
    }
}

export const signUpSubmission = (firstName, lastName, email, phone, password, optInToMarketing)=>{
    //dispatch is available from redux-thunk
    return (dispatch) => {
        axios({
            method:'POST',
            url: '/api/account/signup',
            data: {
                firstName: firstName,
                lastName: lastName,
                email: email,
                phone: phone,
                password: password,
                EmailMarketing: optInToMarketing
            }
        })
        .then((response)=>{
            if (response.status === 200) {
                // console.log(response)
                dispatch(signUpSubmissionSave())
            } 
        })
        .catch((error)=>{
            // console.log(error)
            dispatch(signUpSubmissionError())
        })
    }
}





//////////For User Sign In




export const signInSubmissionSave = (email) => {
    return {
        type: actionTypes.SIGNINSUBMISSION,
        email: email,
    }
}

export const signInSubmissionFail = (email) => {
    return {
        type: actionTypes.SIGNINSUBMISSION_FAIL,
        email: email,
    }
}

export const signInSubmission = (email, password) => {
    return (dispatch)=> {
        
        //make request to log in with email and password from SignInForm
        axios({
            method: 'POST',
            url: '/api/account/signin',
            data: {
                email: email,
                password: password
            }
        })
            .then((response)=>{
                //If response is 200 make request to get user by email
                // console.log(response.data)
                if (response.status ===200) {
                    dispatch(signInSubmissionSave(email, password)); 
                } 
            })
            .catch((error)=>{
                dispatch(signInSubmissionFail(email, password)); 
                console.log(error)
            })
    }
}
//this will return after the axios then call. 

export const getUser = (response) =>{
    return {
        type: actionTypes.SENDUSERTOSTATE,
        response: response,
        firstName: response.data.firstName,
        lastName: response.data.lastName,
        phone: response.data.phone
    }
}

export const getUserRequest= (email) =>{
    return (dispatch)=> {
        axios({
            method: 'GET',
            url: '/api/account/users/'+email
        })
        .then((response)=> {
            dispatch(getUser(response));
        })
        .catch((error)=>{
            console.log(error)
        })
    }
}

