import * as actionTypes from './actions'
import axios from 'axios'

//Think of this as the .then((respone)=>{ })
export const menuFetchDataSave = (response)=>{
    console.log("response from line 6 of ac2: ",response.data);
    return {
        // this Object Will Be Used in the Reducer of choice. The Left hand column is defined by our axios response.
        // Then is accessible in the reducer as action.property. Example (firstName: action.firstName) on the Reducer
        // which we defined as firstName: response.data.firstName
        type: actionTypes.MENU_FETCH_DATA,
        isLoggedIn: true,
        firstName: response.data.user.firstName,
        lastName: response.data.user.lastName,
        email: response.data.user.email,
        phone: response.data.user.phone,
        hardships: response.data.user.Hardships,
        userId: response.data.user._id,
        address: response.data.user.address
    }
};

export const menuFetchData = (token)=> {
    return (dispatch)=> {
        axios ({
            method: 'GET',
            url: '/api/account/token/'+token
        }).then((response)=>{
            console.log(response.data);
            dispatch(menuFetchDataSave(response))
        }).catch((error)=>{
            console.log(error)
        })
    }
};