import React, { Component } from 'react';
import SignUp from './components/User/SignUp/SignUp'
import SignIn from './components/User/SignIn/SignIn'
import Hardship from './components/Hardships/Hardship'
import Account from './components/User/Account/Account'
import Cart from './components/Cart/Cart'
import Header from './components/Header/Header'
import FAQ from './components/Faq/FAQ'
import Menu from './components/Menu/Menu'
import Default from './components/User/Default/Default'
import { connect } from 'react-redux'


class App extends Component {
  render() {
    //if show sigup form is false and show login form is false and not logged in -> show buttons 
    if (!this.props.showSignUpForm && !this.props.showLoginForm && !this.props.isLoggedIn) {
      return (
        <div>
         <div>
        <div className='container-fluid'>
          <Menu/>
          <Header/>
           <div className="row">
             <div className="col-lg-3">
               <Default/>
               <FAQ/>
             </div>
             <div className="col-lg-6">
               <Hardship/>
             </div>
             <div className="col-lg-3">
               <Cart/>
             </div>
           </div>
        </div>
      </div>
        </div>
       );
      // if show login form is true. 
     }else if(this.props.showLoginForm) {
       return (
        <div>
        <div className='container-fluid'>
        <Menu/>
        <Header/>
           <div className="row">
             <div className="col-lg-3">
               <SignIn/>
               <FAQ/>
             </div>
             <div className="col-lg-6">
               <Hardship/>
               <FAQ/>
             </div>
             <div className="col-lg-3">
               <Cart/>
             </div>
           </div>
        </div>
      </div>
       )
     }
     //if show sign up form is true
     else if(this.props.showSignUpForm) {
      return (
       <div>
       <div className='container-fluid'>
       <Menu/>
       <Header/>
          <div className="row">
            <div className="col-lg-3">
              <SignUp/>
              <FAQ/>
            </div>
            <div className="col-lg-6">
              <Hardship/>
              <FAQ/>
            </div>
            <div className="col-lg-3">
              <Cart/>
            </div>
          </div>
       </div>
     </div>
      )
    }
     else if (this.props.isLoggedIn){
       return (
         <div>
           <div className='container-fluid'>
           <Menu/>
           <Header/>
              <div className="row">
                <div className="col-lg-3">
                  <Account/>
                  <FAQ/>
                </div>
                <div className="col-lg-6">
                  <Hardship/>
                </div>
                <div className="col-lg-3">
                  <Cart/>
                </div>
              </div>
           </div>
         </div>
       )
     } 
    }
}

const mapStateToProps = state => {
  return {
    showSignUpForm: state.user.showSignUpForm,
    showLoginForm: state.user.showLoginForm,
    isLoggedIn: state.user.isLoggedIn
  }
}

export default connect(mapStateToProps)(App);
