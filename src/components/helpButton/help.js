import React, {Component} from 'react';
import "./help.css"
import HelpModal from "./helpModal"
class Help extends Component {
    state = {
        open: false
    };
    helpMe(){
      this.setState({
          open:!this.state.open
      });
    }

    render() {
        if (this.state.open){
            return (
                <div>
                    <button onClick={()=>this.helpMe()} className={"btn btn-warning"}>
                        Help
                    </button>
                    <HelpModal/>
                </div>
            );
        }else {
            return (
                <div>
                    <button onClick={()=>this.helpMe()} className={"btn btn-warning"}>
                        Help
                    </button>
                </div>
            );
        }

    }
}

export default Help;