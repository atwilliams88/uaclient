import React, {Component} from 'react';

class HelpModal extends Component {
    state = {
        open: true
    };

    render() {
        if (this.state.open === true) {
            return (
                <div className={"helpBox"}>
                            <div className={"videoTitle"}>How To Make a Donation</div>
                            <iframe width="560" height="315" src="https://www.youtube.com/embed/sgc01j-vKnM"
                                    frameBorder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowFullScreen></iframe>
                    <br/>
                    <hr/>
                    <div className={"videoTitle"}>What is a Tip at Utility Assist</div>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/GJjBPik40aU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    <br/>
                    <hr/>
                    <div className={"videoTitle"}>How To Create An Account?</div>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/nPRdiJ0Fhsw" frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen></iframe>
                    <br/>
                    <hr/>
                    <div className={"videoTitle"}>How To Create Your First Hardship</div>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/ZhpcTQCgXGo" frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen></iframe>
                    <br/>
                    <hr/>
                    <div className={"videoTitle"}>How To Promote Your Hardship to Friends</div>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/NVFf8HLMZOc" frameBorder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen></iframe>
                    <br/>
                    <hr/>
                    <button onClick={() => this.setState({open: false})} className={"btn btn-primary"}>Done</button>
                </div>
            )
        }else {
            return(
                <p className="hide ignore"></p>
            )
        }
    }
}
export default HelpModal;