import React, { Component } from 'react'
import axios from 'axios';

export default class Promote extends Component {
    state = {
        hardships: []
    }
componentDidMount(){
    axios({
        method:'GET',
        
    })
}
  render() {
    return (
      <div>
        {this.state.hardships.map((hardship, i) => (
        <div
              key={i + "div"}
              id="HardshipComponent"
              className="card m-2 p-2 bg-light"
            >
              <CopyToClipboard text={this.state.value}>
              <h5
              onClick={()=>this.getNeedNumberFromClick(hardship._id)} 
              className="mb-3" 
              key={i + "h4"}>
              {"Need Number: " + hardship._id} 
              <i className="fas fa-copy ml-3"></i>
              </h5>
              </CopyToClipboard>
              <div className="row">
                <div className="col col-lg-8 mr-4">
                <h5 className='font-weight-light'>${hardship.MoneyRasied.toFixed()} of ${hardship.Balance.toFixed()} goal</h5>
                  <div 
                  className="progress"
                  style={{width: '100%'}}
                  >
                    <div
                      className="progress-bar" 
                      style={{width: ((hardship.MoneyRasied/hardship.Balance)*100)+'%'}}
                      role="progressbar" 
                      aria-valuenow={(hardship.MoneyRasied)} 
                      aria-valuemin="0" 
                      aria-valuemax={hardship.Balance}>
                      {((hardship.MoneyRasied/hardship.Balance)*100).toFixed(2)+'%'}
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div
                    className="btn-group btn-group-toggle"
                    data-toggle="buttons"
                  >
                    <button
                      className="btn btn-outline-success"
                      onClick={donation =>
                        this.props.addFiveToCart(hardship, 5, i, this.state.transactionGroupId)
                      }
                    >
                      $5
                    </button>
                    <button
                      className="btn btn-outline-success"
                      onClick={donation =>
                        this.props.addTenToCart(hardship, 10,i, this.state.transactionGroupId)
                      }
                    >
                      $10
                    </button>
                    <button
                      className="btn btn-outline-success"
                      onClick={donation =>
                        this.props.addTwentyToCart(hardship, 20,i,this.state.transactionGroupId)
                      }
                    >
                      $20
                    </button>
                  </div>
                </div>
              </div>
              <p className='font-weight-light' key={i + "p2"}>{hardship.Provider}</p>
              <p key={i + "p3"}>{hardship.Description}</p>
              <div className="row text-align-center">
                <div className="col">
                  <form id="donationInputForm">
                    <input
                      id="customAmount"
                      type="number"
                      onChange={e => {
                        this.setState({ customAmount: e.target.value });
                      }}
                      placeholder="Or Enter Custom $ Amount"
                    />
                    <submit
                      id="addCustomDonationButton"
                      onClick={(donation, amount) =>
                        this.props.addCustomToCart(
                          hardship,
                          this.state.customAmount,
                          i,
                          this.state.transactionGroupId
                        )
                      }
                    >
                      Add Custom Amount
                    </submit>
                  </form>
                </div>
              </div>
            </div>
          ))}
      </div>
    )
  }
}
