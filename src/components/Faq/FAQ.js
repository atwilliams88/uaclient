import React, { Component } from "react";
import "./FAQ.css";
export default class FAQ extends Component {
  state= {
    faqTitle:'How long does the process take?',
    faqBody:'All accounts have to be verified. Therefore, the process could take up to 5 business days.'
  };
  showQuestionOne =(e)=>{
   this.setState({
     faqTitle: 'How long does the process take?',
     faqBody: 'All accounts have to be verified. Therefore, the process could take up to 5 business days.'
   })
  };
  showQuestionTwo =(e)=>{
    this.setState({
      faqTitle: 'How much does it cost the consumer?',
      faqBody: 'To publicly request for assistance is free of charge for the consumer.'
    })
   };
   showQuestionThree =(e)=>{
    this.setState({
      faqTitle: 'Can I register for assistance more than once?',
      faqBody: 'As long as the consumer has an active account with a balance due, you may re-register\n' +
          'repeatedly.'
    })
   };
    showQuestionFour =(e)=>{
        this.setState({
            faqTitle: 'Is my donations tax deductible?',
            faqBody: 'Yes. We suggest that you consult with your tax preparer for further assistance.'
        })
    };
    showQuestionFive =(e)=>{
        this.setState({
            faqTitle: 'Will the consumer receive the donations?',
            faqBody: 'No. All donations will go towards the utility company the consumer list for assistance.'
        })
    };
    showQuestionSix =(e)=>{
        this.setState({
            faqTitle: 'Will my information be kept private?',
            faqBody: 'Yes. The consumer and donor’s information will not be displayed to the public.'
        })
    };
    showQuestionSeven =(e)=>{
        this.setState({
            faqTitle: 'How long will my account stay active?',
            faqBody: 'All accounts will remain active for 30 days.  You will receive an email when your account is about to expire'
        })
    };
    showQuestionEight =(e)=>{
        this.setState({
            faqTitle: 'How do you cancel the application?',
            faqBody: 'If the application is incomplete, it will be deleted within 24 hours.'
        })
    };
    showQuestionNine =(e)=>{
        this.setState({
            faqTitle: 'If the information I entered is incorrect or I want to change, how do I re-submit information?',
            faqBody: 'Please email Utility Assist at customerservice@utility-assist.org with the details of the change needed'
        })
    };
    showQuestionTen =(e)=>{
        this.setState({
            faqTitle: 'Will I receive an email that my application has been denied?',
            faqBody: 'Yes'
        })
    };
    showQuestionEleven =(e)=>{
        this.setState({
            faqTitle: 'Can you search by utility company in the search bar on the home page?',
            faqBody: 'No.  But we are working to expand other methods of searches.'
        })
    };
  render() {
    return (
      <div id="faq" className="bg-light">
        <div className="col-lg-10 offset-lg-1 ">
          <div className="form-row">
          <h1 id="faqCta">Frequently Asked</h1>
            <div className="dropdown">
                <button className="btn btn-link dropdown-toggle " type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Questions
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a className="dropdown-item" onClick={this.showQuestionOne}>How long does the process take? </a>
                  <a className="dropdown-item" onClick={this.showQuestionTwo}>How much does it cost the consumer? </a>
                  <a className="dropdown-item" onClick={this.showQuestionThree}>Can I register for assistance more than once?</a>
                  <a className="dropdown-item" onClick={this.showQuestionFour}>Is my donations tax deductible? </a>
                  <a className="dropdown-item" onClick={this.showQuestionFive}>Will the consumer receive the donations? </a>
                  <a className="dropdown-item" onClick={this.showQuestionSix}>Will my information be kept private? </a>
                  <a className="dropdown-item" onClick={this.showQuestionSeven}>How long will my account stay active?</a>
                  <a className="dropdown-item" onClick={this.showQuestionEight}>How do you cancel the application?</a>
                  <a className="dropdown-item" onClick={this.showQuestionNine}>If the information I entered is incorrect or I want to change, how do I re-submit information?</a>
                  <a className="dropdown-item" onClick={this.showQuestionTen}>Will I receive an email that my application has been denied? </a>
                  <a className="dropdown-item" onClick={this.showQuestionEleven}>Can you search by utility company in the search bar on the home page?</a>
                </div>
              </div>
          </div>
          <div className="card">
            <div className="card-body m-0">
              <div className="card-title">
                <h4>{this.state.faqTitle}</h4>
                <p>{this.state.faqBody}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
