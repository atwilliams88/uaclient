import React from 'react'
import { withFormik, Form, Field,} from 'formik';
import  * as Yup from 'yup'
import { connect } from 'react-redux'
import * as actionTypes from '../../../store/actions/actions'
import { signUpSubmission } from '../../../store/actions/actionCreators'
import './SignUpForm.css'
import { Link } from 'react-router-dom'
import axios from 'axios'
import Cookies from 'js-cookie'
import { withRouter } from 'react-router-dom'



const SignUpForm = ({
values,
errors,
touched,
})=> (
  <Form>
    <h1>Create Account</h1>
    {/* This Div Is needed to wrap our error message and our component together */}
    <div>
    { touched.firstName && errors.firstName && <p className="newAlert">{errors.firstName}</p>}
      <Field
      type="text"
      name="firstName"
      className="signupFormInput"
      placeholder="Enter First Name"
      //using Field instead of input allows us to not enter value or onChange it uses the name property to map to inital values defined below
      // value={values.firstName}
      // onChange={handleChange}
      />
    </div>
    <div>
      {touched.lastName && errors.lastName ? <p className="newAlert">{errors.lastName}</p> : ""}
      <Field
        type="text"
        name="lastName"
        className="signupFormInput"
        placeholder="Enter Last Name"
        />
    </div>

    {/* <div>
      {touched.address && errors.address ? <p className="newAlert">{errors.address}</p> : ""}
      <Field
        type="text"
        name="address"
        className="signupFormInput"
        placeholder="Enter Address"
        />
    </div> */}

    <div>
      {touched.email && errors.email ? <p className="newAlert">{errors.email}</p> : ""}
      <Field
      type="email"
      name="email"
      className="signupFormInput"
      placeholder="Enter Email Address"
      />
    </div>
    <div>
      {touched.phone && errors.phone ? <p className="newAlert">{errors.phone}</p> : ""}
      <Field
      type="text"
      name="phone"
      className="signupFormInput"
      placeholder="Enter Phone Number"
      />
    </div>
    <div>
        <p className={"small"}>Password must be at least seven characters long.</p>
      {touched.password && errors.password ? <p className="newAlert">{errors.password}</p> : ""}
      <Field
      type="password"
      name="password"
      className="signupFormInput"
      placeholder="Enter Your Password"
      />
    </div>
    <div>
      {touched.passwordConfirm && errors.passwordConfirm ? <p className="newAlert">{errors.passwordConfirm}</p> : ""}
      <Field
      type="password"
      className="signupFormInput"
      name="passwordConfirm"
      placeholder="Confirm Password"
      />
    </div>
    <br/>
    <button 
    id="createThisButton"
    type="submit">
    Submit</button>
    <Link
    to="/account/login"
    className="btn btn-link"
    id="thisSignInButton"
    >
    Sign In
    </Link>
  </Form>
)
  
  const formikSignUpForm = withRouter(withFormik({
    mapPropsToValues () {
      //inital values
      return {
        firstName: '',
        lastName: '',
        email: '',
        phone:'',
        password:'',
        passwordConfirm:'',
        // address:""
      }
    },
    validationSchema: Yup.object().shape({
      firstName: Yup.string().min(2, 'First Must Be More Than 2 Letters').required('First Name is Required'),
      lastName: Yup.string().min(2,'Last Must Be More Than 2 Letters').required('Last Name is Required'),
      phone: Yup.string().min(10, 'Enter ###-###-####').max(12, 'Enter ###-###-####'),
      email: Yup.string().email('Please Enter Email ').required('Email Is Required'),
      password: Yup.string().min(7).required('Password Is Required'),
      // address: Yup.string().min(7),
      passwordConfirm: Yup.string().oneOf([Yup.ref('password')],'Passwords Do Not Match').required('Confirm Password')
    }),
    //this is done behind the seens with Form component from Formik
    handleSubmit(values, {setErrors, props}) {
      // console.log(values)
      axios({
        method: 'POST',
        url: '/api/account/signup',
        data: {
          firstName: values.firstName,
          lastName: values.lastName,
          email: values.email,
          password: values.password,
          phone: values.phone,
          // address: values.address
        }
      }).then((response)=> {
        // console.log(response.data)
        // console.log(props)
        Cookies.set('UaToken', response.data._id, {expires: 3})
        props.history.push('./overview')
      }).catch((error)=> {
        // console.log(error)
      })
    }

  })(SignUpForm));
  // state = {
  //     firstName: '',
  //     lastName:'',
  //     email: '',
  //     phone:'',
  //     password: '',
  //     optInToMarketing: false
  //   }//
  
  // firstNameChangeHandler = (e)=>{
  //   this.setState ({
  //     firstName: e.target.value
  //   })
  // }

  // lastNameChangeHandler = (e)=>{
  //   this.setState ({
  //     lastName: e.target.value
  //   })
  // }

  // emailChangeHandler = (e)=>{
  //   this.setState ({
  //     email: e.target.value
  //   })
  // }

  // phoneNumberChangeHandler = (e)=>{
  //   this.setState ({
  //     phone: e.target.value
  //   })
  // }

  // passwordChangeHandler = (e)=>{
  //   this.setState ({
  //     password: e.target.value
  //   })
  // }

  // optInToMarket = (e) => {
  //   this.setState({
  //     optInToMarketing: !this.state.optInToMarketing
  //   })
  // }

  // render() {
  //     if(this.props.message) {
  //       return (
  //         <div>
  //           <h1>Sign Up Today</h1>
  //           <div id="newAlert" class="alert alert-danger" role="alert">
  //               <strong>{this.props.message}</strong>
  //             </div>
  //           <Formik 
  //             onSubmit={(values, actions)=>{
  //             console.log(values, actions)
  //             console.log(values.firstName)
  //           }}
  //           render= {({errors, touched})=>(
  //             <Form>
  //               <div className="form-group">
  //                 <Field
  //                 id="f"
  //                 className="form-control" 
  //                 placeholder="First Name"
  //                 name="firstName"
  //                 onChange={e=>{this.firstNameChangeHandler(e)}}
  //                 value={this.state.firstName}
  //                 type="text"
    
  //                 name="firstName">
  //                 </Field>
  //                 {errors.firstName && touched.firstName && <div>{errors.firstName}</div>}
    
  //                 <Field 
  //                 id="l"
  //                 className="form-control" 
  //                 placeholder="Last Name"
  //                 type="text" 
  //                 onChange={e=>{this.lastNameChangeHandler(e)}}
  //                 value={this.state.lastName}
  //                 name="lastName">
  //                 </Field>
  //                 {errors.lastname && touched.lastName && <div>{errors.lastName}</div>}
    
  //                 <Field 
  //                 id="e"
  //                 className="form-control" 
  //                 placeholder="Email Address"
  //                 type="email" 
  //                 onChange={e=>{this.emailChangeHandler(e)}}
  //                 value={this.state.email}
  //                 name="email">
  //                 </Field>
  //                 {errors.email && touched.email && <div>{errors.email}</div>}
    
  //                  <Field 
  //                 id="ph"
  //                 className="form-control" 
  //                 placeholder="Phone Number"
  //                 type="text" 
  //                 onChange={e=>{this.phoneNumberChangeHandler(e)}}
  //                 value={this.state.phone}
  //                 name="phone">
  //                 </Field>
  //                 {errors.phone && touched.phone && <div>{errors.phone}</div>}
    
    
  //                 <Field 
  //                 id="p"
  //                 className="form-control"
  //                 placeholder="Password" 
  //                 type="password" 
  //                 onChange={e=>{this.passwordChangeHandler(e)}}
  //                 value={this.state.password}
  //                 name="password">
  //                 </Field>
  //                 {errors.password && touched.password && <div>{errors.password}</div>}
    
  //                 <div 
  //                 className="form-check">
  //                   <label 
  //                   className='form-check-label' >
  //                   <Field
  //                   id="news" 
  //                   className='form-check-input' 
  //                   type="checkbox" 
  //                   onClick={e=>{this.optInToMarket(e)}}
  //                   name="newsletter"/>
  //                   Join Our News Letter
  //                   </label>
  //                 </div>
  //                 <div className ="row ml-1">
  //                   <button 
  //                   id="createThisButton"
  //                   onClick={()=>this.props.newUserToState(this.state.firstName, this.state.lastName, this.state.email, this.state.phone, this.state.password, this.state.optInToMarketing)}
  //                   type ="submit">
  //                   Create 
  //                   </button>
  //                   <Link 
  //                   to='/account/login'
  //                   id="thisSignInButton"
  //                   className="btn btn-link m-2"
  //                   >Login
  //                   </Link>    
  
  //                 </div>
  //               </div>
  //             </Form>
  //           )}
  //           />
  //         </div>
  //       )
  //     } else {
  //       return (
  //         <div>
  //           <h1>Sign Up Today</h1>
  //           <Formik 
  //             onSubmit={(values, actions)=>{
  //             console.log(values, actions)
    
  //           }}
  //           render= {({errors, touched})=>(
  //             <Form>
  //               <div className="form-group">
  //                 <Field
  //                 id="f"
  //                 className="form-control" 
  //                 placeholder="First Name"
  //                 onChange={e=>{this.firstNameChangeHandler(e)}}
  //                 value={this.state.firstName}
  //                 type="text"
    
  //                 name="firstName">
  //                 </Field>
  //                 {errors.firstName && touched.firstName && <div>{errors.firstName}</div>}
    
  //                 <Field 
  //                 id="l"
  //                 className="form-control" 
  //                 placeholder="Last Name"
  //                 type="text" 
  //                 onChange={e=>{this.lastNameChangeHandler(e)}}
  //                 value={this.state.lastName}
  //                 name="lastName">
  //                 </Field>
  //                 {errors.lastname && touched.lastName && <div>{errors.lastName}</div>}
    
  //                 <Field 
  //                 id="e"
  //                 className="form-control" 
  //                 placeholder="Email Address"
  //                 type="email" 
  //                 onChange={e=>{this.emailChangeHandler(e)}}
  //                 value={this.state.email}
  //                 name="email">
  //                 </Field>
  //                 {errors.email && touched.email && <div>{errors.email}</div>}
    
  //                  <Field 
  //                 id="ph"
  //                 className="form-control" 
  //                 placeholder="Phone Number"
  //                 type="text" 
  //                 onChange={e=>{this.phoneNumberChangeHandler(e)}}
  //                 value={this.state.phone}
  //                 name="phone">
  //                 </Field>
  //                 {errors.phone && touched.phone && <div>{errors.phone}</div>}
    
    
  //                 <Field 
  //                 id="p"
  //                 className="form-control"
  //                 placeholder="Password" 
  //                 type="password" 
  //                 onChange={e=>{this.passwordChangeHandler(e)}}
  //                 value={this.state.password}
  //                 name="password">
  //                 </Field>
  //                 {errors.password && touched.password && <div>{errors.password}</div>}
    
  //                 <div 
  //                 className="form-check">
  //                   <label 
  //                   className='form-check-label' >
  //                   <Field
  //                   id="news" 
  //                   className='form-check-input' 
  //                   type="checkbox" 
  //                   onClick={e=>{this.optInToMarket(e)}}
  //                   name="newsletter"/>
  //                   Join Our News Letter
  //                   </label>
  //                 </div>
  //                 <div className ="row ml-1">
  //                   <button 
  //                   id="createThisButton"
  //                   onClick={()=>this.props.newUserToState(this.state.firstName, this.state.lastName, this.state.email, this.state.phone, this.state.password, this.state.optInToMarketing)}
  //                   type ="submit">
  //                   Create 
  //                   </button>
  //                   <Link 
  //                   to='/account/login'
  //                   id="thisSignInButton"
  //                   className="btn btn-link m-2"
  //                   >Login
  //                   </Link>    
  
  //                 </div>
  //               </div>
  //             </Form>
  //           )}
  //           />
  //         </div>
  //       )
  //     }
      
  // }
// }


const mapStateToProps = state => {
  return {
    hideSignUpForm: state.user.hideSignUpForm,
    message: state.user.errorMessage
  }
}


const mapDispatchToProps = (dispatch)=> {
  return {
    // with actionTypes
    // newUserToState: (firstName, lastName, email)=>dispatch({type: actionTypes.SIGNUPSUBMISSION, firstName: firstName, lastName: lastName, email: email})
    // with actionCreator
    newUserToState: (firstName, lastName, email, phone, password, optInToMarketing)=>dispatch(signUpSubmission(firstName,lastName,email,phone,password,optInToMarketing)),
    openLogin: ()=>dispatch({type: actionTypes.OPEN_LOGIN}),

  }
}

export default connect(mapStateToProps, mapDispatchToProps)(formikSignUpForm)

