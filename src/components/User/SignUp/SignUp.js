import React, { Component } from 'react'
import SignUpForm from './SignUpForm'
import  { connect } from 'react-redux'

class SignUp extends Component {
  render() {
    return (
      <div>
           <div className="container-fluid bg-light" id="signinbg">
          <div className="col-lg-4 offset-lg-5">
          <h2 id='ua'>UTILITY ASSIST</h2>
             <SignUpForm />
          </div>
      </div>
      </div>
    )
  }
}

export default connect()(SignUp)
