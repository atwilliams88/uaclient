import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionTypes from "../../../store/actions/actions";
import "./Sidebar.css";

class Sidebar extends Component {
  // test() {
  //   console.log("click");
  // }

  render() {
    return (
      <div id="sidebar">
        <ul className="list-group">
          <li className="list-group-item myCampaign">
            <i className="far fa-folder" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewMyCampaign()}
            >
              My Hardships
            </button>
          </li>

          <li className="list-group-item myCampaign">
            <i className="fas fa-plus" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewCreateCampaign()}
            >
              Create Hardship
            </button>
          </li>
          <li className="list-group-item myCampaign">
            <i className="fas fa-dollar-sign" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewDonate()}
            >
              Donate To Others
            </button>
          </li>

          <li className="list-group-item myCampaign">
            <i className="far fa-envelope" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewContactUs()}
            >
              Contact Us
            </button>
          </li>
        </ul>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    viewMyCampaign: () => dispatch({ type: actionTypes.VIEW_MY_CAMPAIGN }),
    viewCreateCampaign: () =>
      dispatch({ type: actionTypes.VIEW_CREATE_CAMPAIGN }),
    // viewPromote: () => dispatch({ type: actionTypes.VIEW_PROMOTE }),
    viewDonate: () => dispatch({ type: actionTypes.VIEW_DONATE }),
    viewContactUs: () => dispatch({ type: actionTypes.VIEW_CONTACT_US })
  };
};

export default connect(
  null,
  mapDispatchToProps
)(Sidebar);
