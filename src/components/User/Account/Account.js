import React, { Component } from 'react'
import { connect } from 'react-redux'
import { getUserRequest } from '../../../store/actions/actionCreators' 

class Account extends Component {

componentDidMount(){
    this.getUserData();
}

getUserData = ()=>{
    this.props.sendUserToState(this.props.email)
};

  render() {
    return (
      <div className="box">
        <h1>My Account</h1>
        <hr/>
        <h3>{this.props.firstName}</h3>
        <h3>{this.props.lastName}</h3>
        <h3>{this.props.email}</h3>
      </div>
    )
  }
}
const mapStateToProps = state => {
    return {
        firstName: state.user.firstName,
        lastName: state.user.lastName,
        email: state.user.email,
    }
};

const mapDispatchToProps = dispatch => {
    return {
        sendUserToState: (email)=>dispatch(getUserRequest(email))
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Account)
