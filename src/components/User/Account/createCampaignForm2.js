import React, {Component} from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import "./ccf.css"
import * as actionTypes from "../../../store/actions/actions";
class CreateCampaignForm2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            file: "",
            firstName: "",
            lastName: "",
            streetAddress: "",
            city:"",
            state:"",
            zip:"",
            utilityCompany: "",
            accountNumber: "",
            currentBalance: "",
            dueDate: "",
            description: "",
            errorCount: "",
            previewMode: false,
            required: new RegExp('[a-zA-Z]'),
            inFocus: "",
            requirements:"",
            limitReached: false
        }
    }
    inputFocus(e, field) {
        console.log("focus on: ", field, "value: ", e.target.value)
    }

    handleSubmit(e) {
        if (this.state.description.length<281){
            console.log("dueDate: ", this.state.dueDate);
            console.log("owner: ", this.props.userId)
            axios({
                method: "POST",
                url: "/api/hardship",
                data: {
                    Owner: this.props.userId,
                    Location: this.state.streetAddress + "/ "+ this.state.city + "/ "+ this.state.state + "/ "+ this.state.zip,
                    Description: this.state.description,
                    Balance: this.state.currentBalance,
                    AccountNumber: this.state.accountNumber,
                    Provider: this.state.utilityCompany,
                    firstName: this.state.firstName,
                    lastName: this.state.lastName,
                    dueDate: this.state.dueDate,
                    FilterId: Math.random()*1000,
                }
            }).then(response=> {
                console.log(response)
            })
                .catch(error=> {
                    console.log(error)
                });
            this.props.viewMyCampaign();
            e.preventDefault();
        }else {
            e.preventDefault();
            this.setState({
                limitReached: true
            })
        }

    }
    validateResponse (e, field){
        const check =field.exec(e.target.value);
        if (check === null) {
           console.log("Did Not Pass Check")
        } else {
            console.log("Passed Check")
        }
        // this.state.validation.+f.exec(e.target.value);
        // console.log(this.state.validation.+field+.exec(e.target.value))
    }
    render() {
        if(this.state.previewMode===false && this.state.limitReached===false){
        return (
            <div>
                <h1>Create Hardship</h1>
                <hr/>
                <p>You may create a hardship for yourself or others. Please enter information as accurately as possible.<br/>
                    We strongly encourage you to attach a bill or invoice to the hardship application.
                </p>
                <p>After your application is submitted it will be reviewed by the Utility Assist Team for authenticity. <br/>
                   Please allow up to three business days for review. You may check the status of the application on under "My Hardships".
                </p>
                    <h3>Enter Name:</h3>
                    <form onSubmit={(e)=> this.handleSubmit(e)}>
                            <input
                                type="text"
                                name="firstName"
                                value={this.state.firstName}
                                onFocus={(e)=> this.inputFocus(e, "firstName")}
                                onBlur={(e)=> this.validateResponse(e, this.state.required)}
                                placeholder="First Name"
                                onChange={(e)=>this.setState({
                                    firstName: e.target.value
                                })}
                                />
                            <input
                            type="text"
                            name="lastName"
                            value={this.state.lastName}
                            placeholder="Last Name"
                            onChange={(e)=>this.setState({
                                lastName: e.target.value
                            })}
                        />
                    <br/>
                    <h3>Address Details:</h3>
                    <input
                        type="text"
                        name="streetAddress"
                        value={this.state.streetAddress}
                        placeholder="Street Address"
                        onChange={(e)=>this.setState({
                            streetAddress: e.target.value
                        })}
                    />  <input
                    type="text"
                    name="city"
                    value={this.state.city}
                    placeholder="City"
                    onChange={(e)=>this.setState({
                        city: e.target.value
                    })}
                />  <input
                    type="select"
                    name="state"
                    value={this.state.state}
                    placeholder="State"
                    onChange={(e)=>this.setState({
                        state: e.target.value
                    })}
                />  <input
                    type="number"
                    name="zip"
                    value={this.state.zip}
                    placeholder="Zip"
                    onChange={(e)=>this.setState({
                        zip: e.target.value
                    })}
                />
                <br/>
                    <h3>Billing Details:</h3>
                    <input
                    type="text"
                    name="utilityCompany"
                    value={this.state.utilityCompany}
                    placeholder="Utility Company"
                    onChange={(e)=>this.setState({
                        utilityCompany: e.target.value
                    })}
                />  <input
                    type="text"
                    name="accountNumber"
                    value={this.state.accountNumber}
                    placeholder="Account Number"
                    onChange={(e)=>this.setState({
                        accountNumber: e.target.value
                    })}
                />  <input
                    type="text"
                    name="currentBalance"
                    value={this.state.currentBalance}
                    placeholder="Current Balance"
                    onChange={(e)=>this.setState({
                        currentBalance: e.target.value
                    })}

                    /> <label className={"ddLabel"}>Hardship Due Date: (must be in the future)<input className={"ddInput"}
                    type="date"
                    name="dueDate"
                    value={this.state.dueDate}
                    placeholder="Due Date"
                    onChange={(e)=>this.setState({
                        dueDate: e.target.value
                    })}
                    />
                    </label>
                    <br/>
                    <h3>Hardship Narrative: </h3>
                    <p>The narrative will be shared with potential donors and the public. The rest of your information is kept private. </p>

                    <textarea
                        type="textarea"
                        rows="4"
                        cols="75"
                        name="description"
                        placeholder="Description"
                        onChange={(e)=>this.setState({
                            description: e.target.value
                        })}
                    />
                        <div className={"wordCount"}>
                            {this.state.description.length} / 280
                        </div>
                    <br/>
                    <h3>Please Attach Supporting Documents:</h3>
                        <p>(PDF preferred)</p>
                        <input
                        className="fileInput"
                        type="file"
                        name="file"
                        onChange={(e)=>this.setState({
                            file: e.target.files[0]
                        })}
                    />
                    <br/>
                <hr/>
                    <div class="previewButtonRow">
                        <button
                        className="createHardshipButton"
                        onClick={()=> this.setState({previewMode: !this.state.previewMode})}
                        >Preview</button>
                    <br/>
                        <button
                            className="createHardshipButton"
                            type="submit"

                        >Submit</button>
                    </div>
                </form>
            </div>
        )}
        if(this.state.previewMode===true){
            return(
                <div>
                    <h1>Preview Hardship</h1>
                    <div className="card m-2 p-2 bg-light">
                        <h4>Need Number: TBD </h4>
                        <hr/>
                        <h5>Hardship Status: Pending</h5>
                        <p>
                            <strong>Starting Balance: {this.state.currentBalance }</strong>
                        </p>
                        <p>Raised To Date: $0</p>
                        <h5>Due Date:{this.state.dueDate}</h5>
                        <p>Provider {this.state.utilityCompany}</p>
                        <p>Description {this.state.description}</p>
                    </div>
                    <hr/>
                    <div className="previewButtonRow">
                        <button
                            className="createHardshipButton"
                            onClick={() => this.setState({previewMode: !this.state.previewMode})}
                        >Back
                        </button>
                        <br/>
                        <button
                            className="createHardshipButton"
                            type="submit"
                            onClick={e => this.handleSubmit(e)}
                        >Submit
                        </button>
                    </div>
                </div>
            )
        } if (this.state.previewMode===false && this.state.limitReached===true){
            return (
                <div>
                    <h1>Create Hardship</h1>
                    <hr/>
                    <p>You may create a hardship for yourself or others. Please enter information as accurately as possible.<br/>
                        We strongly encourage you to attach a bill or invoice to the hardship application.
                    </p>
                    <p>After your application is submitted it will be reviewed by the Utility Assist Team for authenticity. <br/>
                        Please allow up to three business days for review. You may check the status of the application on under "My Hardships".
                    </p>
                    <h3>Enter Name:</h3>
                    <form onSubmit={(e)=> this.handleSubmit(e)}>
                        <input
                            type="text"
                            name="firstName"
                            value={this.state.firstName}
                            onFocus={(e)=> this.inputFocus(e, "firstName")}
                            onBlur={(e)=> this.validateResponse(e, this.state.required)}
                            placeholder="First Name"
                            onChange={(e)=>this.setState({
                                firstName: e.target.value
                            })}
                        />
                        <input
                            type="text"
                            name="lastName"
                            value={this.state.lastName}
                            placeholder="Last Name"
                            onChange={(e)=>this.setState({
                                lastName: e.target.value
                            })}
                        />
                        <br/>
                        <h3>Address Details:</h3>
                        <input
                            type="text"
                            name="streetAddress"
                            value={this.state.streetAddress}
                            placeholder="Street Address"
                            onChange={(e)=>this.setState({
                                streetAddress: e.target.value
                            })}
                        />  <input
                        type="text"
                        name="city"
                        value={this.state.city}
                        placeholder="City"
                        onChange={(e)=>this.setState({
                            city: e.target.value
                        })}
                    />  <input
                        type="select"
                        name="state"
                        value={this.state.state}
                        placeholder="State"
                        onChange={(e)=>this.setState({
                            state: e.target.value
                        })}
                    />  <input
                        type="number"
                        name="zip"
                        value={this.state.zip}
                        placeholder="Zip"
                        onChange={(e)=>this.setState({
                            zip: e.target.value
                        })}
                    />
                        <br/>
                        <h3>Billing Details:</h3>
                        <input
                            type="text"
                            name="utilityCompany"
                            value={this.state.utilityCompany}
                            placeholder="Utility Company"
                            onChange={(e)=>this.setState({
                                utilityCompany: e.target.value
                            })}
                        />  <input
                        type="text"
                        name="accountNumber"
                        value={this.state.accountNumber}
                        placeholder="Account Number"
                        onChange={(e)=>this.setState({
                            accountNumber: e.target.value
                        })}
                    />  <input
                        type="text"
                        name="currentBalance"
                        value={this.state.currentBalance}
                        placeholder="Current Balance"
                        onChange={(e)=>this.setState({
                            currentBalance: e.target.value
                        })}
                    />
                        <label className={"ddLabel"}>Hardship Due Date: (must be in the future)
                            <input className={"ddInput"}
                              type="date"
                              name="dueDate"
                              value={this.state.dueDate}
                              placeholder="Due Date"
                              onChange={(e)=>this.setState({
                                  dueDate: e.target.value
                              })}
                        />
                        </label>
                        <br/>
                        <h3>Hardship Narrative: </h3>
                        <p>The narrative will be shared with potential donors and the public. The rest of your information is kept private. </p>
                        <p class="errorMessage">Hardship Narrative Max Length is 280 Characters</p>
                        <textarea
                            type="textarea"file
                            rows="4"
                            cols="75"
                            name="description"
                            placeholder="Description"
                            onChange={(e)=>this.setState({
                                description: e.target.value
                            })}
                        />
                        <div className={"wordCount"}>
                            {this.state.description.length} / 280
                        </div>
                        <br/>
                        <h3>Please Attach Supporting Documents:</h3>
                        <p>(PDF preferred)</p>
                        <input
                            className="fileInput"
                            type="file"
                            name="file"
                            onChange={(e)=>this.setState({
                                file: e.target.files[0]
                            })}
                        />
                        <br/>
                        <hr/>
                        <div class="previewButtonRow">
                            <button
                                className="createHardshipButton"
                                onClick={()=> this.setState({previewMode: !this.state.previewMode})}
                            >Preview</button>
                            <br/>
                            <button
                                className="createHardshipButton"
                                type="submit"

                            >Submit</button>
                        </div>
                    </form>
                </div>
            )
        }
    }
}
const mapStateToProps = (state)=> {
    return {
        userId:state.user.userId
    }
};
const mapStateToDispatch = (dispatch)=> {
    return {
        viewMyCampaign: ()=>dispatch({type: actionTypes.VIEW_MY_CAMPAIGN}),
    }
};
export default connect(mapStateToProps, mapStateToDispatch)(CreateCampaignForm2);