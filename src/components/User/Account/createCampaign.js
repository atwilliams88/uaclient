import React, { Component } from 'react'
import createCampaignForm from './createCampaignForm'

class createCampaign extends Component {
    state = {
        for:""
    };

forSelf () {
    this.setState({
        for:'self'
    })
}

forSomeoneElse() {
    this.setState({
        for: 'someoneElse'
    })
}

  render() {
    if(!this.state.for) {
        return (
            <div>
              <div className="row">
                  <button
                  onClick={()=>this.forSelf()}
                  >Self</button>
                  <button
                  onClick={()=>this.forSomeoneElse()}
                  >Someone Else</button>
              </div>
            </div>
          )
    } else if (this.state.for ==="self"){
        return (
            <div>
                <h1>Self</h1>
                <createCampaignForm/>
            </div>
        )
    }else {
        return (
            <div>
                <h1>Someone Else</h1>
                <createCampaignForm/>
            </div>
        )
    }
  }
}

export default(createCampaign)
