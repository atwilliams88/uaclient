import React, {Component} from 'react';

import axios from 'axios';

class Upload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedFile: "",
        }
    }
    sendFile(e) {
        console.log("SendMethod from Upload", this.state.selectedFile)
        axios({
            method: "POST",
            url: "/api/hardship/documents",
            data: {
                name: this.state.selectedFile.name,
                file: this.state.selectedFile.file,
                size: this.state.selectedFile.size,
                lastMod: this.state.selectedFile.lastModified
            }
        }).then(response=> {
            console.log(response)
        }).catch(error=> {
            console.log(error)
        });
        e.preventDefault();
    }
    render() {
        return (
            <form ref='uploadForm'
                  id='uploadForm'
                  action='/api/hardship/documents'
                  method='post'
                  encType="multipart/form-data">
                <input type="file" name="sampleFile" />
                <input type='submit' value='Upload!' />
            </form>

        );
    }
}

export default Upload;