import React from 'react'
import { withFormik, Form, Field,  } from 'formik'
import * as Yup from 'yup'
import { withRouter } from 'react-router-dom'
import Upload from './Upload'
import axios from 'axios'
import './createCampaignForm.css'
import { connect } from 'react-redux'
import * as actionTypes from '../../../store/actions/actions'

const createCampaignForm = ({
    touched,
    errors
}) =>(
    <div>
        <h1>Create Hardship</h1>
        <hr/>

        <h5>Contact Information should match attached bill or notice</h5>
        <Form className="form-group">
            <div className="form-row">
                <div className="col col-lg-2">
                    { touched.firstName && errors.firstName && <p className="newAlert">{errors.firstName}</p>}
                    <Field 
                    type="text"
                    name="firstName"
                    className="form-control"
                    placeholder="Enter First Name"
                    />
                </div>
                <div className="col col-lg-3">
                    { touched.lastName && errors.lastName ? <p className="newAlert">{errors.lastName}</p> : ""}
                    <Field 
                    type="text"
                    name="lastName"
                    className="form-control"
                    placeholder="Enter Last Name"
                    />
                </div>
            </div>
            <div className="form-row mt-2">
                <div className="col col-lg-6">
                    { touched.street && errors.street && <p className="newAlert">{errors.street}</p>}
                    <Field
                    type="text"
                    name="street"
                    className="form-control"
                    placeholder="Street"
                    />
                </div>
                <div className="col col-lg-3">
                    { touched.city && errors.city && <p className="newAlert">{errors.city}</p>}
                    <Field
                    type="text"
                    name="city"
                    className="form-control"
                    placeholder="City"
                    />
                </div>
                <div className="col col-lg-2">
                    { touched.state && errors.state && <p className="newAlert">{errors.state}</p>}
                    <Field
                    component="select"
                    className="form-control"
                    name="state">
                    <option value="MO">MO</option>
                    <option value="IL">IL</option>
                    </Field>
                </div>
                <div className="col col-lg-1">
                    { touched.zip && errors.zip && <p className="newAlert">{errors.zip}</p>}
                    <Field
                    type="Number"
                    name="zip"
                    className="form-control"
                    placeholder="Zip"
                    />
                </div>
            </div>
            <h3 className="mt-4">Billing Information</h3>
            <div className="form-row">
                <div className="col col-lg-3">
                    { touched.Provider && errors.Provider && <p className="newAlert">{errors.Provider}</p>}
                    <Field
                    component="select"
                    className="form-control"
                    name="Provider">
                    <option value="Ameren">Ameren</option>
                    <option value="Spire">Spire</option>
                    <option value="Stl. Sewer District">Stl. Sewer District (MSD)</option>
                    <option value="Stl. City Water">Stl. City Water</option>
                    <option value="Stl. Coutny Water">Stl. County Water</option>
                    <option value="Stl. City Real Estate Taxes">Stl. City Real Estate Taxes</option>
                    <option value="Stl. County Real Estate Taxes">Stl. County Real Estate Taxes</option>
                    <option value="Stl. City Trash">Stl. City Trash</option>
                    <option value="Stl. County Trash">Stl. County Trash</option>
                    </Field>
                </div>
                <div className="col col-lg-3">
                    { touched.AccountNumber && errors.AccountNumber && <p className="newAlert">{errors.AccountNumber}</p>}
                    <Field
                    type="text"
                    name="AccountNumber"
                    className="form-control"
                    placeholder="Enter Account Number"
                    />
                </div>
                <div className="col col-lg-3">
                    { touched.Balance && errors.Balance && <p className="newAlert">{errors.Balance}</p>}
                    <Field
                    type="Number"
                    name="Balance"
                    className="form-control"
                    placeholder="Enter Your Balance"
                    />
                </div>   
                <div className="col col-lg-3">
                    { touched.DueDate && errors.DueDate && <p className="newAlert">{errors.DueDate}</p>}
                    <Field
                    type="Date"
                    name="DueDate"
                    className="form-control"
                    placeholder="When Is Your Bill Due"
                    />
                </div> 
            </div>
            <h3 className="mt-4">Hardship Description</h3>
            { touched.Description && errors.Description && <p className="newAlert">{errors.Description}</p>}
            <Field
            component='textarea'
            name="Description"
            rows="5"
            className="form-control mt-2"
            placeholder="This information will be visible to donors. However, your identity will remain anonymous"
            />
            <h3>Please Attach Invoice or Letter for account confirmation</h3>
            <p>You may create a hardship for yourself or others. However, we would like a copy of a bill or notice to verify the account.</p>
            <input
                type="file"
                name="file"
                />
            <hr/>
            <button
            type="submit"
            id="createHardshipButton"
            >
            Create Campaign
            </button>
        </Form>
    </div>
);

const mapStateToProps = state=> {
    return {
        userId:state.user.userId
    }
};

const mapDispatchToProps = dispatch =>{
    return {
        viewMyCampaign: ()=>dispatch({type: actionTypes.VIEW_MY_CAMPAIGN}),
    }
};

const formikCreateCampaignForm = connect(mapStateToProps, mapDispatchToProps)(withRouter(withFormik({
mapPropsToValues(){
    return {
        Description: '',
        DueDate: '',
        Balance: '',
        AccountNumber: '',
        Provider: 'Ameren',
        firstName:'',
        lastName:'',
        city:'',
        state:'MO',
        zip:'',
        street:'',
        file:''
    }
},
validationSchema:Yup.object().shape({
    firstName: Yup.string().min(2, 'First Must Be More Than 2 Letters').required('First Name is Required'),
    lastName: Yup.string().min(2,'Last Must Be More Than 2 Letters').required('Last Name is Required'),
    street: Yup.string().required(),
    city: Yup.string().required(),
    state: Yup.string().required().min(2).max(2),
    zip: Yup.number().required(),
    Provider: Yup.string().required(),
    AccountNumber: Yup.string(),
    Balance: Yup.number().positive(),
    DueDate: Yup.date(),
    Description: Yup.string().max(350).required()
}),

handleSubmit(values, {resetForm, setStatus, props}) {
// console.log(values, props)
    console.log(values)
axios({
    method: 'POST',
    url: '/api/hardship',
    data: {
        Owner: props.userId,
        FirstName: values.firstName,
        LastName: values.lastName,
        Location: values.street +" "+values.city+" "+values.state+" "+values.zip,
        Provider: values.Provider,
        AccountNumber: values.AccountNumber,
        Balance: values.Balance,
        DueDate: values.DueDate,
        Description: values.Description,
        FilterId: Math.random()*1000
    }
}).then((response)=> {
    if(response.status ===200) {
        setStatus({ success: true });
        resetForm()
        props.viewMyCampaign()
    }
}).catch((error)=> {
    // console.log(error)
})

}

})(createCampaignForm)))

export default formikCreateCampaignForm