import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import Cookies from 'js-cookie'
import "./MyCampign.css";
import * as actionTypes from '../../../store/actions/actions'

class MyCampaign extends Component {
  state = {
    hardships: "",
  };

componentDidMount(){
    this.getHardships()
}

  getHardships() {
    var id = Cookies.get('UaToken')
    axios({
      method: "GET",
      url: "/api/hardships/id/" + id
    })
      .then(response => {
        // console.log(response.data)
        this.setState({
          hardships: response.data.hardship
        });
      })
      .catch(error => {
        // console.log(error);
      });
  }
  render() {
    if (!this.state.hardships) {
      return (
        <div>
          <h1>My Hardships</h1>
            <h2>{this.props.owner}</h2>
        </div>
      );
    } else {
      return (
        <div id="myCampaign" className="container-fluid">
            <div className="row">   
                <h1 className="col col-lg-3 mt-2">My Hardships</h1>
                <button 
                className="btn btn-link col-lg-3 offset-lg-6 mt-2" 
                id="plusLink"
                onClick={()=>this.props.viewCreateCampaign()}   
                data-toggle="tooltip"
                data-placement="top" title="Create New Hardship">
                <i className="fas fa-plus-circle" />
                    Create Hardship
                </button>

            </div>
          <hr />
          {this.state.hardships.map((hardship, i) => (
            <div key={i + "div"} className="card m-2 p-2 bg-light">
              <h4 key={i + "h4"}>{"Need Number: " + hardship._id}</h4>
              <hr key={i + "hr"} />
              <h5 key={i + "hp"}>{"Status: " + hardship.Status}</h5>
              <p key={i + "p"}>
                <strong>{"Starting Balance: " + "$" + hardship.Balance}</strong>
              </p>
              <p>Raised To Date: $ {hardship.MoneyRasied}</p>
              <h5 key={i + "h5"}>{"Due Date: " + hardship.DueDate}</h5>
              <p key={i + "p2"}>{"Provider: " + hardship.Provider}</p>
              <p key={i + "p3"}>{"Description: " + hardship.Description}</p>
            </div>
          ))}
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {
      owner: state.user.userId
  }
}

const mapDispatchToProps = dispatch => {
  return {
    viewCreateCampaign: ()=>dispatch({type: actionTypes.VIEW_CREATE_CAMPAIGN}),
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(MyCampaign);
