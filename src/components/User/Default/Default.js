import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as actionTypes from '../../../store/actions/actions'

class Default extends Component {
  render() {
    return (
      <div>
        <h1>Welcome</h1>
        <div className="row ml-1">

            <button 
            className="btn btn-primary m-2"
            onClick={()=>this.props.openLogin()}
            >Login
            </button>

            <button 
            className="btn btn-secondary m-2"
            onClick={this.props.openCreateAccount}
            >Create Account
            </button>

        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
    return {
        openLogin: ()=>dispatch({type: actionTypes.OPEN_LOGIN}),
        openCreateAccount: ()=>dispatch({type: actionTypes.OPEN_CREATE_ACCOUNT})
    }
}

const mapStateToProps = state => {
    return {

    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Default)
