import React from 'react'

import { connect } from 'react-redux'
import {  withFormik, Form, Field,} from 'formik';
import * as Yup from 'yup'
import Cookies from 'js-cookie'
import * as actionTypes from '../../../store/actions/actions'
import { signInSubmission } from '../../../store/actions/actionCreators'
import './singInForm.css'
import axios from 'axios'
import { Link, withRouter} from 'react-router-dom'


const SignInForm = ({
    values,
    errors,
    touched,
})=> (
   <Form>
       <h1>Please Sign In</h1>
        <div>
            {touched.email && errors.email && <p className="newAlert">{errors.email}</p> }
            <Field
            className="signupFormInput"
            type="email"
            name="email"
            placeholder="Enter Email Address"
            />
        </div>
        <div>
            {touched.password && errors.password && <p className="newAlert">{errors.password}</p> }
            <Field
            type="password"
            name="password"
            className="signupFormInput"
            placeholder="Enter Your Password"
            />
        </div>
      <br/>
      <button
      id="signInButton"
      type="submit">
        Sign In
      </button>
      <Link
      to="/account/create"
      className="btn btn-link"
      id="createAccountButton"
      >
      Create Account
      </Link>
   <br/>
       <div id="message"></div>
       {/*<p className={"forgot"}*/}
        {/*onClick={()=>console.log("reset password")}*/}
       {/*>reset password</p>*/}
   </Form>
);

const formikSignInForm = withRouter(withFormik({
    mapPropsToValues () {
        return {
            email:'',
            password:''
        }
    },
    validationSchema: Yup.object().shape({
        email: Yup.string().email('Please Enter Email ').required('Email Is Required'),
        password: Yup.string().min(7).required('Password Is Required')
    }),
    handleSubmit(values, {setErrors, props}) {
        // console.log(values)
        // console.log(props)
        axios({
            method: 'POST',
            url: '/api/account/signin',
            data: {
                email: values.email,
                password: values.password
            }
        }).then((response)=>{
            if(response.status===406){
                console.log("406 Bad Password")
            }
            // console.log(response.data)
            Cookies.set('UaToken', response.data.token, {expires: 3});
            props.history.push('./overview');
            if(response.data.Admin ===true) {
                Cookies.set('UaAdminToken', response.data.token, {expires: 1});
            }
            console.log("from signIn: ",response.data)
        })
        .catch((error)=> {
            console.log(error.response.data.message);
            document.getElementById("message").innerText=error.response.data.message;
            setErrors({email: 'Username or Password was wrong'})
        })
    }
})(SignInForm));

export default formikSignInForm
    