import React, { Component } from 'react'
import SignInForm from './SignInForm'
import { connect } from 'react-redux'
import './singInForm.css'

class SignIn extends Component {
  render() {
    if(!this.props.message) {
      return (
        <div className="container-fluid bg-light" id="signinbg">
          <div className="col-lg-4 offset-lg-5">
          <h2 id='ua'>UTILITY ASSIST</h2>
             <SignInForm test="abc"/>
          </div>
      </div>
      )
    } else {
      return (
        <div className="container-fluid bg-light" id="signinbg">
          <div className="col-lg-4 offset-lg-5">
          <h2 id='ua'>UTILITY ASSIST</h2>
            <div class="alert alert-danger" role="alert">
              <strong>{this.props.message}</strong> - Bad Email or Password
            </div>
            <SignInForm />
         </div>
      </div>
      )
    }
  }
}

const mapStateToProps = state=> {
  return {
    message: state.user.errorMessage
  }
}

export default connect(mapStateToProps)(SignIn) 