import React, { Component } from 'react'
import Cookies from 'js-cookie'
import { connect } from 'react-redux'
import { withRouter, Link } from 'react-router-dom'
import './UserMenu.css'

class UserMenu extends Component {

signOutRequest (history) {
    Cookies.remove('UaToken');
    history.push("/")
}
//
  render() {
    var history = this.props.history;
    if(this.props.isLoggedIn === false) {
        return (
            <h1>Please Sign In</h1>
        )
    } else {
        return (
            <div className="row">
                  <div
                  id="userNavLeft"
                  className="col"
                  >
                  <h3>{this.props.firstName} {this.props.lastName}</h3>
                  </div>
                  <div
                  id="userNavCenter"
                  className="col"
                  >
                  {/* h5 search is a placeholder */}
                  <Link 
                  id='HomeButtonUserMenu'
                  to="/">Home
                  </Link>
                  </div>
                  <div
                  id="userNavRight"
                  className="col"
                  >
                  <button 
                  id="signOutButton"
                  className="btn btn-link"
                  onClick={()=>this.signOutRequest(history)}
                  >
                  Sign Out
                  </button>
                  </div>
            </div>
          )
    } 
  }
}

const mapDispatchToProps = dispatch => {
    return {
        // getUserData2: (token)=>dispatch({type:actionTypes.MENU_FETCH_DATA, token: token}),
        // getUserData2: (token)=>dispatch(menuFetchData(token))
        // newUserToState: (firstName, lastName, email, phone, password, optInToMarketing)=>dispatch(signUpSubmission(firstName,lastName,email,phone,password,optInToMarketing)),
    }
}

const mapStateToProps = state => {
    return {
        firstName: state.user.firstName,
        lastName: state.user.lastName,
        isLoggedIn: state.user.isLoggedIn
    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(UserMenu))
