import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionTypes from "../../store/actions/actions";
import axios from "axios";
import {CopyToClipboard} from 'react-copy-to-clipboard';
import "./Hardship.css";

class Hardship extends Component {
  state = {
    hardships: "",
    customAmount: 0,
    transactionGroupId: '',
    value:'',
  };
  componentDidMount() {
    this.getAllHardships();
    this.generateId();
  }

  getNeedNumberFromClick(needNumber) {
    this.setState({
      value: needNumber,
    })
    // <CopyToClipboard text=needNumber/>
  }
  generateId(){
    var transactionGroupId = Math.floor(Math.random()*10000000000000000);
    this.setState({
      transactionGroupId: transactionGroupId
    })
  }
 
  getAllHardships = () => {
    axios({
      method: "GET",
      url: "/api/hardships"
    }).then(response => {
        console.log(response.data);
      this.setState({
        hardships: response.data.hardship
      });
    });
  };
  render() {
    if (!this.state.hardships) {
      return (
        <div>
          <h1>Hardship Component Waiting</h1>
        </div>
      );
    } else {
      return (
        <div className="container-fluid">
          <h1>Hardships </h1>
          {this.state.hardships.map((hardship, i) => (
            <div
              key={i + "div"}
              id="HardshipComponent"
              className="card m-2 p-2 bg-light"
            >
              <CopyToClipboard text={this.state.value}>
              <h5
              onClick={()=>this.getNeedNumberFromClick(hardship._id)} 
              className="mb-3" 
              key={i + "h4"}>
              {"Need Number: " + hardship._id} 
              <i className="fas fa-copy ml-3"></i>
              </h5>
              </CopyToClipboard>
              <div className="row">
                <div className="col col-lg-8 mr-4">
                <h5 className='font-weight-light'>${hardship.MoneyRasied.toFixed()} of ${hardship.Balance.toFixed()} goal</h5>
                  <div 
                  className="progress"
                  style={{width: '100%'}}
                  >
                    <div
                      className="progress-bar" 
                      style={{width: ((hardship.MoneyRasied/hardship.Balance)*100)+'%'}}
                      role="progressbar" 
                      aria-valuenow={(hardship.MoneyRasied)} 
                      aria-valuemin="0" 
                      aria-valuemax={hardship.Balance}>
                      {((hardship.MoneyRasied/hardship.Balance)*100).toFixed(2)+'%'}
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div
                    className="btn-group btn-group-toggle"
                    data-toggle="buttons"
                  >
                    <button
                      className="btn btn-outline-success"
                      onClick={donation =>
                        this.props.addFiveToCart(hardship, 5, i, this.state.transactionGroupId)
                      }
                    >
                      $5
                    </button>
                    <button
                      className="btn btn-outline-success"
                      onClick={donation =>
                        this.props.addTenToCart(hardship, 10,i, this.state.transactionGroupId)
                      }
                    >
                      $10
                    </button>
                    <button
                      className="btn btn-outline-success"
                      onClick={donation =>
                        this.props.addTwentyToCart(hardship, 20,i,this.state.transactionGroupId)
                      }
                    >
                      $20
                    </button>
                  </div>
                </div>
              </div>
              <p className='font-weight-light' key={i + "p2"}>{hardship.Provider}</p>
              <p key={i + "p3"}>{hardship.Description}</p>
              <div className="row text-align-center">
                <div className="row ml-3">
                  {/*<form id="donationInputForm">*/}
                    <input
                      className="col"
                      id="customAmount"
                      type="number"
                      onChange={e => {
                        this.setState({ customAmount: e.target.value });
                      }}
                      placeholder="Custom Amount"
                    />
                    <button
                      id="addCustomDonationButton"
                      onClick={(donation, amount) =>
                        this.props.addCustomToCart(
                          hardship,
                          this.state.customAmount,
                          i,
                          this.state.transactionGroupId
                        )
                      }
                    >
                      Add Custom Amount
                    </button>
                  {/*</form>*/}
                </div>
              </div>
            </div>
          ))}
        </div>
      );
    }
  }
}

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    addFiveToCart: (hardship, donation,i,transactionGroupId) =>
      dispatch({
        type: actionTypes.ADD_FIVE_TO_CART,
        payload: {
          hardship,
          donation,
          i,
          transactionGroupId,
        }
      }),
    addTenToCart: (hardship, donation,i,transactionGroupId) =>
      dispatch({
        type: actionTypes.ADD_TEN_TO_CART,
        payload: {
          hardship,
          donation,
          i,
          transactionGroupId
        }
      }),
    addTwentyToCart: (hardship, donation,i,transactionGroupId) =>
      dispatch({
        type: actionTypes.ADD_TWENTY_TO_CART,
        payload: {
          hardship,
          donation,
          i,
          transactionGroupId
        }
      }),
    addCustomToCart: (hardship, donation,i,transactionGroupId) =>
      dispatch({
        type: actionTypes.ADD_CUSTOM_TO_CART,
        payload: {
          hardship,
          donation,
          i,
          transactionGroupId
        }
      })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Hardship);
