import React, { Component } from 'react'
import './Register.css'
import { Link } from 'react-router-dom'

export default class Register extends Component {
  render() {
    return (
      <Link id="LinkStyle" to="/account/create"><div id="registerBox" className="justify-content-center mainSpacing bg-light">
        <div className="col-lg-10 offset-lg-1 ">
            <h1 id="registerCta">Register</h1>
            <p>Need help with your utility bills?  Click here to display your hardship for assistance to the community.
                Once you start the application be sure to write down your hardship ID #.  You would need this as a quick reference to view only your hardships.  Also, you may give your hardship ID # to friends and family.  Or, you may rely on the community for assistance.  The public will only know you by your hardship ID #.
                For instructions on how to apply click the “Help” button at the top of the homepage.
            </p>
        </div>
      </div>
      </Link>
    )
  }
}
