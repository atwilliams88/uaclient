import React, { Component } from 'react'
import { withRouter, Link } from 'react-router-dom'
import Cookies from 'js-cookie'
import axios from 'axios'
class AdminMenu extends Component {
state = {
    owner: "",
    foundUser: ""
};
    signOutRequest () {
    Cookies.remove('UaToken');
    Cookies.remove('UaAdminToken');
    this.props.history.push("/")
}
findEmail(e){
        e.preventDefault();
        axios({
            method: "GET",
            url: "/api/account/token/"+this.state.owner
        }).then((response)=> {
            console.log(response.data);
            this.setState({
                foundUser: response.data
            })
        })
}

render() {
    if (!this.state.foundUser){
        return (
            <div className="row">
                <div
                    id="userNavLeft"
                    className="col col-lg-3"
                >
                    <h3>Admin</h3>
                </div>
                <div
                    id="userNavCenter"
                >
                    {/* h5 search is a placeholder */}
                    <Link
                        placeholder={"Enter User ID"}
                        id='HomeButtonUserMenu'
                        to="/">Home
                    </Link>
                    <form onSubmit={(e)=>this.findEmail(e)}>
                        <input
                            onChange={(e)=> this.setState({owner: e.target.value})}
                            type="text"/>
                        <button>Find User</button>
                    </form>
                </div>
                <div
                    id="userNavRight"
                >
                    <button
                        id="signOutButton"
                        className="btn btn-link"
                        onClick={()=>this.signOutRequest()}
                    >
                        Sign Out
                    </button>
                </div>
            </div>
        )
    }else {
        return (
            <div className="row">
                <div
                    id="userNavLeft"
                    className="col col-lg-3"
                >
                    <h3>Admin</h3>
                </div>
                <div
                    id="userNavCenter"
                >
                    {/* h5 search is a placeholder */}
                    <Link
                        id='HomeButtonUserMenu'
                        to="/">Home
                    </Link>
                    <form onSubmit={(e)=>this.findEmail(e)}>
                        <input
                            onChange={(e)=> this.setState({owner: e.target.value})}
                            type="text"/>
                        <button
                            class="btn btn-secondary"
                        >Find User</button>
                    </form>
                    <div>
                        First: {this.state.foundUser.user.firstName}<br/>
                        Last: {this.state.foundUser.user.lastName}<br/>
                        Email: <a className="mailTo" href={"mailto:"+this.state.foundUser.user.email}>{this.state.foundUser.user.email}</a><br/>
                        Phone: {this.state.foundUser.user.phone}
                    </div>
                </div>
                <div
                    id="userNavRight"
                >
                    <button
                        id="signOutButton"
                        className="btn btn-link"
                        onClick={()=>this.signOutRequest()}
                    >
                        Sign Out
                    </button>
                </div>
            </div>
        )
    }

  }
}
export default withRouter(AdminMenu)
