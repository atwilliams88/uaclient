import React, { Component } from 'react'
import { connect } from 'react-redux'
import axios from 'axios'
import drop from 'lodash.drop'
import head from 'lodash.head'

class adminPayments extends Component {
    state = {
        allUnprocessedTransactions:[],
        transaction:"",
        tg:"",
        index:0
    };
    componentDidMount(){
     this.loadAllTransactions()
    }
    loadAllTransactions(){
        axios({
            method: "GET",
            url:"/api/transactiongroup"
        }).then((response)=> {
          console.log("all transactions need to split into array: ", response)
            let holdingArray=[];
            response.data.group.forEach(function(tg){
                holdingArray.push(tg.transactionGroupId)
            })
            this.setState({
                allUnprocessedTransactions: holdingArray
            })
        }).then(()=>{
            console.log("array:", this.state.allUnprocessedTransactions)
            this.loadTransaction()
        })

    }
    loadTransaction(){
        axios({
            method: "GET",
            url: "/api/unprocessed/groups/"+ this.state.allUnprocessedTransactions[this.state.index]
        }).then((response)=> {
            console.log("matching Transactions 41", response.data.matchingTransactions)
            console.log("whereitBreaks: ",response.data)
            console.log("matching tg", response.data.matchingTg )
            this.setState({
                transaction: response.data.matchingTransactions,
                tg: response.data.matchingTg
            })
        }).catch((error)=> {
            console.log(error)
        })
        this.setState({
            index: parseInt(this.state.index)+1
        })
    }
    next(){
        this.setState({
            index: parseInt(this.state.index)+1
        })
        this.loadTransaction();
    }
    makeProcessed(){
        axios({
            method: "POST",
            url:"/api/transactiongroup/processed/"+this.state.allUnprocessedTransactions[this.state.index]
        }).then((response)=>{
          console.log("status: ",response.status)
        })
    }
    completeTransaction(){
        this.next()
        this.makeProcessed()
    }

    render(){
        if(this.state.transaction && this.state.tg){
            return (
                <div>
                    <h1>Process Payments</h1>
                    <hr/>
                    <h3>Transactions To Process: {this.state.allUnprocessedTransactions.length}</h3>
                    {this.state.tg.map((g)=>(
                            <div>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Transaction Group ID</th>
                                            <th>Need Number</th>
                                            <th>Provider</th>
                                            <th>Provider Account #</th>
                                            <th>Amount </th>
                                            <th>User </th>
                                            <th>Processed</th>
                                            <th>Date of Donation</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{g.transactionGroupId}</td>
                                            <td>Group Master</td>
                                            <td>None</td>
                                            <td>None</td>
                                            <td>${(parseInt(g.amount)/100).toLocaleString("en",{
                                                currency:"USD"
                                            })}</td>
                                            <td><a className="mailTo" href={"mailto:"+ g.donor}>{g.donor}</a></td>
                                            <td>{g.processed.toString()}</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    <tbody>
                                    {this.state.transaction.map((t)=>(
                                    <tr onClick={(e)=>e.target.style.backgroundColor="#FFFF07"}>
                                            <td>{t.transactionGroupId}</td>
                                            <td>{t.needNumber}</td>
                                            <td>{t.provider}</td>
                                            <td>{t.accountNumber}</td>
                                            <td>${parseInt(t.moneyRaised).toLocaleString("en","USD")}</td>
                                            <td>{t.owner}</td>
                                            <td>{t.appliedToBill.toString()}</td>
                                            <td>{t.transactionDate}</td>
                                        </tr>
                                        ))}
                                    </tbody>
                                </table>
                            </div>
                        ))}
                    <div className="btn btn-primary mb-2"
                         onClick={()=>this.completeTransaction()}
                    >Complete</div><br/>
                    <div
                        className="btn btn-warning"
                        onClick={()=>this.next()}

                        >Skip</div>
                </div>
            )
        }else {
            return(
                <div>
                    <h1>Please Reload Page</h1>
                </div>
            )
        }

    }
}
export default adminPayments
