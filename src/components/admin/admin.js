import React, { Component } from 'react'
import axios from 'axios'
import Cookies from 'js-cookie'
import { withRouter } from 'react-router-dom'
import AdminMenu from './adminMenu/adminMenu'
import AdminSideDrawer from './adminSideDrawer/adminSideDrawer'
import AdminPendingHardship from './adminHardships/adminPendingHardships'
import AdminDeniedHardship from './adminHardships/adminDeniedHardships'
import AdminApprovedHardship from './adminHardships/adminApprovedHardships'
import AdminPayments from './adminPayments/adminPayments'
import { connect } from 'react-redux'
class Admin extends Component {
    state = {
        authenticated: false
    };

componentDidMount() {
    this.isAdmin()
}

isAdmin(){
   var adminCookie=Cookies.get('UaAdminToken');
    axios ({
        method: 'POST',
        url:'/admin/token',
        data: {
            adminToken: adminCookie
        }
    }).then((response)=>{
        this.setState({
            authenticated: response.data.valid
        })
    }).catch((error)=> {
        // console.log(error)
    })
}

  render() {
    if(this.state.authenticated===true && this.props.page==="pendingHardships" || !this.props.page) {
        // if (this.props.page === "myCampaign") {
            return (
              <div className="container-fluid">
                <AdminMenu />
                <div className="row">
                  <div id="sbg" className="col col-lg-3">
                    <AdminSideDrawer />
                  </div>
                  <div id="userMainSection"className="col col-lg-9">
                    <AdminPendingHardship/>
                  </div>
                </div>
              </div>
            );
    } else if (this.state.authenticated===true && this.props.page==="deniedHardships") {
        return (
            <div className="container-fluid">
              <AdminMenu />
              <div className="row">
                <div id="sbg" className="col col-lg-3">
                  <AdminSideDrawer />
                </div>
                <div id="userMainSection"className="col col-lg-9">
                    <AdminDeniedHardship/>
                </div>
              </div>
            </div>
          );
    }else if (this.state.authenticated===true && this.props.page==="approvedHardships") {
        return (
            <div className="container-fluid">
              <AdminMenu />
              <div className="row">
                <div id="sbg" className="col col-lg-3">
                  <AdminSideDrawer />
                </div>
                <div id="userMainSection"className="col col-lg-9">
                    <AdminApprovedHardship/>
                </div>
              </div>
            </div>
          );
    }else if (this.state.authenticated===true && this.props.page==="Donations") {
        return (
            <div className="container-fluid">
              <AdminMenu />
              <div className="row">
                <div id="sbg" className="col col-lg-3">
                  <AdminSideDrawer />
                </div>
                <div id="userMainSection"className="col col-lg-9">
                <AdminPayments/>
                </div>
              </div>
            </div>
          );
    }else if (this.state.authenticated===true && this.props.page==="donorList") {
        return (
            <div className="container-fluid">
              <AdminMenu />
              <div className="row">
                <div id="sbg" className="col col-lg-3">
                  <AdminSideDrawer />
                </div>
                <div id="userMainSection"className="col col-lg-9">
                <h1>Placeholder for Donor List</h1>
                </div>
              </div>
            </div>
          );
    }else if (this.state.authenticated===true && this.props.page==="stats") {
        return (
            <div className="container-fluid">
              <AdminMenu />
              <div className="row">
                <div id="sbg" className="col col-lg-3">
                  <AdminSideDrawer />
                </div>
                <div id="userMainSection"className="col col-lg-9">
                <h1>Placeholder for Stats</h1>
                </div>
              </div>
            </div>
          );
    }else {
        return (
            <div className="container-fluid">
              <AdminMenu />
              <div className="row">
                <div id="sbg" className="col col-lg-3">
                  <AdminSideDrawer />
                </div>
                <div id="userMainSection"className="col col-lg-9">
                  <h1>Please Refresh Or Login Again</h1>
                </div>
              </div>
            </div>
          );
    }
  }
}

const mapStateToProps = state => {
    return {
        page: state.sidebar.page
    }
}

export default withRouter(connect(mapStateToProps)(Admin))