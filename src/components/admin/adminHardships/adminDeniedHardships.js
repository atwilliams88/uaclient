import React, { Component } from "react";
import axios from "axios";
import "./adminHardships.css";
class AdminDeniedHardships extends Component {
  state = {
      hardships: [],
      messageView: false,
      contact: "",

  };
  componentDidMount() {
    this.getDeniedHardships();
  }

  // denyHardship(id) {
  //   axios({
  //     method: "PUT",
  //     url: "/api/hardships/deny",
  //     data: {
  //       id: id
  //     }
  //   }).then(response => {
  //     // console.log(response);
  //   });
  // }
    messageClient(id) {
      this.setState({
          messageView: true
      });
      axios ({
          method: "GET",
          url: '/api/account/token/'+id
      }).then(response=> {
          console.log(response);
          this.setState({
              contact: response
          })
        }).catch(error=> {
          console.log(error)
        })
    }

  approveHardship(id) {
    axios({
      method: "PUT",
      url: "/api/hardships/approve",
      data: {
        id: id
      }
    }).then(response => {
      // console.log(response);
    });
  }

  getDeniedHardships() {
    axios({
      method: "GET",
      url: "/api/hardships/DeniedStatus"
    })
      .then(response => {
        this.setState({
          hardships: response.data.hardship
        });
      })
      .catch(error => {
        // console.log(error);
      });
  }
  render() {
    if (this.state.messageView===false) {
        return (
            <div className="container-fluid">
                <h1 bg-warning>Denied Hardships</h1>
                {this.state.hardships.map((hardship, i) => (
                    <div>
                        <div
                            key={i + "div"}
                            id="HardshipComponent"
                            className="card m-2 p-2 bg-light"
                        >
                            <div className="row">
                                <div className="col col-lg-3">
                                    <h5 className="mb-3" key={i + "h4"}>
                                        {"Need Number: " + hardship._id}
                                    </h5>
                                </div>
                                <div className="col col-lg-3 offset-lg-3">
                                    {/*<button*/}
                                        {/*className="btn btn-outline-danger"*/}
                                        {/*onClick={() => this.messageClient(hardship._id)}*/}
                                    {/*>*/}
                                        {/*Message*/}
                                    {/*</button>*/}
                                </div>
                            </div>

                            <div className="row">
                                <div className="col col-lg-8 mr-4">
                                    <h5 className="font-weight-light">
                                        ${hardship.MoneyRasied.toFixed()} of $
                                        {hardship.Balance.toFixed()} goal
                                    </h5>
                                    <div className="progress" style={{ width: "100%" }}>
                                        <div
                                            className="progress-bar"
                                            style={{
                                                width:
                                                    (hardship.MoneyRasied / hardship.Balance) * 100 + "%"
                                            }}
                                            role="progressbar"
                                            aria-valuenow={hardship.MoneyRasied}
                                            aria-valuemin="0"
                                            aria-valuemax={hardship.Balance}
                                        >
                                            {(
                                                (hardship.MoneyRasied / hardship.Balance) *
                                                100
                                            ).toFixed(2) + "%"}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <ul>
                                <li>
                                    <strong>User: </strong>
                                    {hardship.Owner}
                                </li>
                                <li>
                                    <strong>Provider: </strong>
                                    {hardship.Provider}
                                </li>
                                <li>
                                    <strong>Account Number: </strong>
                                    {hardship.AccountNumber}
                                </li>
                                <li>
                                    <strong>Location: </strong>
                                    {hardship.Location}
                                </li>
                                <li>
                                    <strong>Due Date: </strong>
                                    {hardship.DueDate}
                                </li>
                            </ul>
                            <p key={i + "p3"}>{hardship.Description}</p>
                            <div className="row text-align-center" />
                        </div>
                    </div>
                ))}
            </div>
        );
    } else if (this.state.messageView===true) {
      return (
          <div>
              <h1>Message View</h1>
          </div>
      )
    }

  }
}

export default AdminDeniedHardships;
