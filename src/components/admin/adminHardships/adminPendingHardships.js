import React, { Component } from "react";
import axios from "axios";
import "./adminHardships.css";
class AdminHardships extends Component {
  state = {
    hardships: [],
    editMode: false,
    editHardship: {},
    provider: "",
    accountNumber: "",
    location:"",
    dueDate:"",
    description:""
  };
  componentDidMount() {
    this.getPendingHardships();
  }

  denyHardship(id) {
    axios({
      method: "PUT",
      url: "/api/hardships/deny",
      data: {
        id: id
      }
    }).then(response => {
      // console.log(response);
    });
  }

  approveHardship(id) {
    axios({
      method: "PUT",
      url: "/api/hardships/approve",
      data: {
        id: id
      }
    }).then(response => {
      // console.log(response);
    });
  }

  getPendingHardships() {
    axios({
      method: "GET",
      url: "/api/hardships/pending"
    })
      .then(response => {
        this.setState({
          hardships: response.data.hardship
        });
      })
      .catch(error => {
        // console.log(error);
      });
  }

  editMode(id) {
    // console.log(id);
    axios({
      method: "GET",
      url: "/api/hardship/id/" + id
    })
      .then(response => {
        // console.log(response);
        this.setState({
          editMode: true,
          editHardship: response.data.hardship[0]
        });
      })
      .catch(error => {
        // console.log(error);
      });
  }
  updateHardship () {
    axios({
        method: "PUT",
        url: "/api/hardship/updateById",
        data: {
          provider: this.state.provider,
          accountNumber: this.state.accountNumber,
          location: this.state.location,
          dueDate: this.state.dueDate,
          hardship: this.state.editHardship._id,
          description: this.state.description
        }
    });
      this.setState({
          editMode: false
      })
  }

  render() {
    if (this.state.editMode === false) {
      return (
        <div className="container-fluid">
          <h1>Pending Hardships</h1>
          {this.state.hardships.map((hardship, i) => (
            <div>
              <div
                key={i + "div"}
                id="HardshipComponent"
                className="card m-2 p-2 bg-light"
              >
                <div className="row">
                  <div className="col col-lg-3">
                    <h5 className="mb-3" key={i + "h4"}>
                      {"Need Number: " + hardship._id}
                    </h5>
                  </div>
                  <div className="col col-lg-3 offset-lg-3">
                    <div
                      className="btn-group btn-group-toggle"
                      data-toggle="buttons"
                    >
                      <button
                        className="btn btn-outline-danger"
                        onClick={() => this.denyHardship(hardship._id)}
                      >
                        Deny
                      </button>
                      <button
                        onClick={() => this.editMode(hardship._id)}
                        className="btn btn-outline-warning"
                      >
                        Edit
                      </button>
                      <button
                        className="btn btn-outline-success"
                        onClick={() => this.approveHardship(hardship._id)}
                      >
                        Approve
                      </button>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col col-lg-8 mr-4">
                    <h5 className="font-weight-light">
                      ${hardship.MoneyRasied.toFixed()} of $
                      {hardship.Balance.toFixed()} goal
                    </h5>
                    <div className="progress" style={{ width: "100%" }}>
                      <div
                        className="progress-bar"
                        style={{
                          width:
                            (hardship.MoneyRasied / hardship.Balance) * 100 +
                            "%"
                        }}
                        role="progressbar"
                        aria-valuenow={hardship.MoneyRasied}
                        aria-valuemin="0"
                        aria-valuemax={hardship.Balance}
                      >
                        {(
                          (hardship.MoneyRasied / hardship.Balance) *
                          100
                        ).toFixed(2) + "%"}
                      </div>
                    </div>
                  </div>
                </div>
                <ul>
                  <li>
                    <strong>User: </strong>
                    {hardship.Owner}
                  </li>
                  <li>
                    <strong>Provider: </strong>
                    {hardship.Provider}
                  </li>
                  <li>
                    <strong>Account Number: </strong>
                    {hardship.AccountNumber}
                  </li>
                  <li>
                    <strong>Location: </strong>
                    {hardship.Location}
                  </li>
                  <li>
                    <strong>Due Date: </strong>
                    {hardship.DueDate}
                  </li>
                </ul>
                <p key={i + "p3"}>{hardship.Description}</p>
                <div className="row text-align-center" />
              </div>
            </div>
          ))}
        </div>
      );
    } else if (this.state.editMode === true) {
      return (
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-4">
              <div className="card m-3 p-3">
                <h1>Current Value</h1>
                  <h2>Hardship #: </h2><p className="card-title">{this.state.editHardship._id}</p>
                <ul className="card-body">
                  <li>
                    <strong>User: </strong>
                    {this.state.editHardship.Owner}
                  </li>
                  <li>
                    <strong>Provider: </strong>
                    {this.state.editHardship.Provider}
                  </li>
                  <li>
                    <strong>Account Number: </strong>
                    {this.state.editHardship.AccountNumber}
                  </li>
                  <li>
                    <strong>Location: </strong>
                    {this.state.editHardship.Location}
                  </li>
                  <li>
                    <strong>Due Date: </strong>
                    {this.state.editHardship.DueDate}
                  </li>
                </ul>
                <textarea
                cols="50"
                rows="4"
                >{this.state.editHardship.Description}</textarea>
              </div>
            </div>

            <div className="col-lg-8">
              <div className="card m-3 p-3">
                <h1>Updated Value</h1>
                  <h2>Hardship #: </h2><p className="card-title">{this.state.editHardship._id}</p>
                <form>
                <ul className="card-body">
                  <li>
                    <strong>User: </strong>
                      {this.state.editHardship.Owner}
                  </li>
                  <li >
                    <strong>Provider: </strong>
                    <input
                      type="text"
                      onChange={event => this.setState({provider:event.target.value})}
                      className="editProvider"
                      placeholder="Provider"
                      />
                  </li>
                  <li >
                    <strong>Account Number: </strong>
                    <input
                    className="editAccountNumber"
                    onChange={event => this.setState({accountNumber:event.target.value})}
                    type="text"
                    placeholder="Account Number"
                    />
                  </li>
                  <li>
                    <strong>Location: </strong>
                    <input
                    className="editLocation"
                    onChange={event => this.setState({location:event.target.value})}
                    type="text"
                    placeholder="Location"
                    />
                  </li>
                  <li>
                    <strong>Due Date: </strong>
                    <input
                        className="editDueDate"
                        onChange={event => this.setState({dueDate:event.target.value})}
                        type="date"
                        placeholder="Due Date"
                        />
                  </li>
                </ul>
                  <form>
                    <textarea
                      onChange={event => this.setState({description:event.target.value})}
                      cols="50"
                      rows="4"
                    />
                  </form>
                </form>
                </div>
                <button class="btn btn-success" onClick={()=>this.updateHardship()}>Update</button>
                <button className="btn btn-danger" onClick={()=> this.setState({editMode: false})}>Cancel</button>
            </div>
            </div>
        </div>
      );
    }
  }
}

export default AdminHardships;
