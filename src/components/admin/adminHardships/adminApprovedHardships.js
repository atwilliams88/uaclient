import React, { Component } from "react";
import axios from "axios";
import "./adminHardships.css";
class AdminApprovedHardships extends Component {
  state = {
    hardships: [],
  };
  componentDidMount() {
    this.getPendingHardships();
  }

  denyHardship(id) {
    axios({
      method: "PUT",
      // url: "/api/hardships/deny",
        url: "/api/hardships/deny",

        // url: "/api/hardships/deny",
      data: {
        id: id
      }
    }).then(response => {
      // console.log(response);
    });
  }

  approveHardship(id) {
    axios({
      method: "PUT",
      url: "/api/hardships/approve",
      data: {
        id: id
      }
    }).then(response => {
      // console.log(response);
    });
  }

  getPendingHardships() {
    axios({
      method: "GET",
      url: "/api/hardships/ApprovedStatus"
    })
      .then(response => {
        this.setState({
          hardships: response.data.hardship
        });
      })
      .catch(error => {
        // console.log(error);
      });
  }
  render() {
    return (
      <div className="container-fluid">
      <h1 bg-warning>Approved Hardships</h1>
        {this.state.hardships.map((hardship, i) => (
          <div>
            <div
              key={i + "div"}
              id="HardshipComponent"
              className="card m-2 p-2 bg-light"
            >
              <div className="row">
                <div className="col col-lg-3">
                  <h5 className="mb-3" key={i + "h4"}>
                    {"Need Number: " + hardship._id}
                  </h5>
                </div>
                <div className="col col-lg-3 offset-lg-3">
                  <div
                    className="btn-group btn-group-toggle"
                    data-toggle="buttons"
                  >
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col col-lg-8 mr-4">
                  <h5 className="font-weight-light">
                    ${hardship.MoneyRasied.toFixed()} of $
                    {hardship.Balance.toFixed()} goal
                  </h5>
                  <div className="progress" style={{ width: "100%" }}>
                    <div
                      className="progress-bar"
                      style={{
                        width:
                          (hardship.MoneyRasied / hardship.Balance) * 100 + "%"
                      }}
                      role="progressbar"
                      aria-valuenow={hardship.MoneyRasied}
                      aria-valuemin="0"
                      aria-valuemax={hardship.Balance}
                    >
                      {(
                        (hardship.MoneyRasied / hardship.Balance) *
                        100
                      ).toFixed(2) + "%"}
                    </div>
                  </div>
                </div>
              </div>
              <ul>
                <li>
                  <strong>User: </strong>
                  {hardship.Owner}
                </li>
                <li>
                  <strong>Provider: </strong>
                  {hardship.Provider}
                </li>
                <li>
                  <strong>Account Number: </strong>
                  {hardship.AccountNumber}
                </li>
                <li>
                  <strong>Location: </strong>
                  {hardship.Location}
                </li>
                <li>
                  <strong>Due Date: </strong>
                  {hardship.DueDate}
                </li>
              </ul>
              <p key={i + "p3"}>{hardship.Description}</p>
              <div className="row text-align-center" />
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default AdminApprovedHardships;
