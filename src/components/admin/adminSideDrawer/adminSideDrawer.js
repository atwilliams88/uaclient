import React, { Component } from "react";
import { connect } from "react-redux";
import * as actionTypes from "../../../store/actions/actions";
import "./adminSideDrawer.css";

class AdminSideDrawer extends Component {
  render() {
    return (
      <div id="sidebar">
        <ul className="list-group">
          <li className="list-group-item myCampaign">
            <i className="far fa-folder" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewPendingHardships()}
            >
              Pending Hardships
            </button>
          </li>
          <li className="list-group-item myCampaign">
            <i class="fas fa-times-circle" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewDeniedHardships()}
            >
              Denied Hardships
            </button>
          </li>
          <li className="list-group-item myCampaign">
            <i class="fas fa-check" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewApprovedHardships()}
            >
              Approved Hardships
            </button>
          </li>

          <li className="list-group-item myCampaign">
            <i className="fas fa-dollar-sign" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewDonations()}
            >
              Donations
            </button>
          </li>

          <li className="list-group-item myCampaign">
          <i class="fas fa-users"></i>           
           <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewDonorList()}
            >
              Donors List
            </button>
          </li>

          <li className="list-group-item myCampaign">
            <i class="fas fa-trophy" />
            <button
              className="btn btn-link myCampaign"
              onClick={() => this.props.viewStats()}
            >
              Key Statistics
            </button>
          </li>
        </ul>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    viewPendingHardships: () =>
      dispatch({ type: actionTypes.VIEW_PENDING_HARDSHIPS }),
    viewApprovedHardships: () =>
      dispatch({ type: actionTypes.VIEW_APPROVED_HARDSHIPS }),
    viewDeniedHardships: () =>
      dispatch({ type: actionTypes.VIEW_DENIED_HARDSHIPS }),
    viewDonorList: () => dispatch({ type: actionTypes.VIEW_DONOR_LIST }),
    viewDonations: () => dispatch({ type: actionTypes.VIEW_DONATIONS }),
    viewStats: () => dispatch({ type: actionTypes.VIEW_STATS })
  };
};

export default connect(
  null,
  mapDispatchToProps
)(AdminSideDrawer);
