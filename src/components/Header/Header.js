import React, { Component } from "react";
import Menu from "../../components/Menu/Menu";
import "./Header.css";

class Header extends Component {
  render() {
    return (
      <div id="mainHeader" className="container-fluid">
          <Menu/>
        <h1 id="cta">
          UTILITY ASSIST 
        </h1>
        <h5 id="headerH2">Helping Others with Utility Payments and More in St.Louis City & County</h5>
        <div className="container">
          <div>
            <div id="iconRow" className="row mt-4">
              <h2 className="ml-3"><i className="fas fa-tint"></i></h2>
              <h2><i className="fas fa-bolt"></i></h2>
              <h2><i className="fas fa-trash"></i></h2>
              <h2><i className="fas fa-home"></i></h2>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
