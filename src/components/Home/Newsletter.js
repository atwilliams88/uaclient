import React, { Component } from "react";
import axios from 'axios'
import {Formik} from 'formik'
import "./NewsLetter.css";


export default class Newsletter extends Component {
state = {
  submitted:false,
}
  render() {
    if( this.state.submitted === false) {
      return (
        <div className="bg-light" id="newsletter">
          <div className="col-lg-10 offset-lg-1">
            <h1 id="newsletterCTA">Join Our Newsletter <i id="ev" className="far fa-envelope-open"></i></h1>
            <p>Stay connected to Utility Assist by getting a quarterly email about whats happening at Utility Assist </p>
            <div>
              <Formik
              initialValues={
                {
                  'firstName': '',
                  'lastName': '',
                  'email':''
                }
              }
              onSubmit={(values)=>{
                // console.log(values)
                axios({
                  method: "POST",
                  url:'/newsletter',
                  data: {
                    firstName: values.firstName,
                    lastName: values.lastName,
                    email: values.email
                  }
                })
                this.setState({
                  submitted:true
                })
              }}
              render={props =>(
                <form onSubmit={props.handleSubmit}>
                  <div className="row">
                    <input
                    name="firstName"
                    id="first"
                    placeholder="First Name"
                    type="text"
                    value={props.values.firstName}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    />
                    <input
                    name="lastName"
                    id="last"
                    placeholder="Last Name"
                    type="text"
                    value={props.values.lastName}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    />
                  </div>
                  <input
                    name="email"
                    id="email"
                    placeholder="Enter Email Address"
                    type="email"
                    value={props.values.email}
                    onChange={props.handleChange}
                    onBlur={props.handleBlur}
                    />
                  <button 
                  id="newsletterButton"
                  type="submit">Submit
                  </button>
                </form>
              )}
              >
              </Formik>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className="container mt-2 ml-2 bg-light" id="newsletter">
          <div className="col-lg-10 offset-lg-1">
            <h1 id="newsletterCTA">Thank You <i className="fas fa-check"></i></h1>
            <p>You have sucessfully joined our newsletter</p>
          </div>
        </div>
      )
    }
    
  }
}
