import React, { Component } from "react";
import { Link } from "react-router-dom";
import Cookies from "js-cookie";
import "./Menu.css";
import Search from "./Search";
import { withRouter } from "react-router-dom";
import Help from "../helpButton/help"
class Menu extends Component {
  render() {
    var token = Cookies.get("UaToken");
    if (!token && this.props.location.pathname === "/") {
      return (
            <div className="flexTry">
                <p>
                  <Link className ="menuItem" id="loginButton" to="/account/login">
                    Login
                  </Link>
                </p>
                <p>
                  <Link className="menuLink menuItem" to="/account/create">
                    Register
                  </Link>
                </p>
                <p>
                  <Link to="/needs" className="menuLink menuItem">
                    Donate
                  </Link>
                </p>
                <Help/>
                <div className="menuSearch">
                  <Search/>
                </div>
            </div>
          );
        } if (this.props.location.pathname==="/" && token){
          return (
            <div className="flexTry">
                <p>
                  <Link
                  id="loginButton"
                  to="account/overview">
                  My Account
                  </Link>
                </p>
                <p>
                  <Link to="/needs" className="menuLink">
                    Donate
                  </Link>
                </p>
                <Help/>
                <div className="menuSearch">
                  <Search/>
                </div>
              </div>
          )
    }
    if (this.props.location.pathname === "/needs" && token) {
      return (
        <div className="flexTry">
          <p>
            <Link id="loginButton" to="account/overview">
              My Account
            </Link>
          </p>
          <p>
            <Link to="/" className="menuLink">
              Home
            </Link>
          </p>
            {/*<button className={"btn btn-warning"}>Help</button>*/}
          <div className="menuSearch">
            <Search />
          </div>
        </div>
      );
    }
    if (this.props.location.pathname === "/needs" && !token) {
      return (
        <div className="flexTry">
          <p>
            <Link id="loginButton" to="/account/login">
              Login
            </Link>
          </p>
          <p>
            <Link className="menuLink" to="/account/create">
              Register
            </Link>
          </p>
          <p>
            <Link to="/" className="menuLink">
              Home
            </Link>
          </p>
            <div className="menuSearch">
            <Search />
          </div>
        </div>
      );
    } else {
      return (
        <div className="flexTry">
          <p>
            <Link id="loginButton" to="/" className="menuLink">
              Home
            </Link>
          </p>
          <div className="menuSearch">
            <Search />
          </div>
        </div>
      );
    }
  }
}

export default withRouter(Menu);
