import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class Search extends Component {
  state = {
    input: ""
  };
  find() {
    this.props.history.push(/needs/ + this.state.input);
  }
  render() {
    return (
    <div className="search">
        <div className="form-group formSize">
          <div className="input-group">
            <div className="input-group-prepend">
              <span className="input-group-text">
                <i className="fas fa-search" />
                <input
                  type="text"
                  id="searchInput"
                  placeholder="Find By Hardship #"
                  onChange={e =>
                    this.setState({
                      input: e.target.value
                    })
                  }
                />
                <button
                  className="btn btn-outline-success"
                  type="button"
                  onClick={() => this.find()}
                  id="button-addon2"
                >
                  Search
                </button>
              </span>
            </div>
          </div>
        </div>
    </div>
    );
  }
}
export default withRouter(Search);
