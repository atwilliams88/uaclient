import React, { Component } from "react";
import { connect } from "react-redux";
import "./Cart.css";
import * as actionTypes from "../../store/actions/actions";
import StripeCheckout from "react-stripe-checkout";
import axios from "axios";

class Cart extends Component {
  state={
    transactionComplete:false
  };
  onToken = token => {
    axios({
      method: "POST",
      url: "/save-stripe-token",
      body: token
    }).then(response => {
      var email = response.config.body.email;
      // var fisrtName = response.config.body.firstName
      axios({
        method: "POST",
        url: "/save-data",
        data: {
          source: response.config.body.id,
          amount: this.props.basicAmount,
          currency: "usd",
          description: "test",
          hardships: this.props.dollars,
          transactionGroupId: this.props.transactionGroupId,
          email: email
        }
      })
        .then(response => {
          // console.log(response);
          // if (response.status ===200) {
          //   this.setStatus({
          //     transactionComplete: true,
          //   })
          // }
        })
        .then(() => {
          axios({
            method: "POST",
            url: "/save-transaction",
            data: {
              amount: this.props.stripeAmount,
              transactionGroupId: this.props.transactionGroupId,
              donor: email
            }
          })
            .then(response => {
              // console.log(response);
            })
            .catch(error => {
              // console.log(error);
            });
        });
    });
  };
  render() {
    if (this.props.tip === 0 && this.state.transactionComplete===false) {
      return (
        <div id="stickyCart">
          <h1>Your Cart</h1>
          <div className="jumbotron" id="cartJumbo">
            <div className="form-row mr-5">
              <div className="col">
                <p className="lead ">Count</p>
              </div>
              <div className="col">
                <p className="lead ">Donation Amount</p>
              </div>
              <div className="col">
                <p className="lead">Need Number</p>
              </div>
            </div>
            {this.props.dollars.map((dollar, i) => (
              <div id="cartRow" className="form-row mr-5">
                <div className="col">
                  {i + 1}
                  <button
                    className="btn btn-link"
                    onClick={() => {
                      this.props.removeDonation(
                        dollar.hardship._id,
                        dollar.donation,
                        i,
                        dollar.filterId
                      );
                    }}
                  >
                    <i className="fas fa-times" />
                  </button>
                </div>
                <div className="col">
                  <p>$ {dollar.donation}</p>
                </div>
                <div className="col">
                  <p>
                    ...
                    {dollar.hardship._id.slice(16, 25)}
                  </p>
                </div>
              </div>
            ))}
            <h2 className="mt-2">Your Donation: ${this.props.total}</h2>
          </div>
          <div id="tipSection" className="jumbotron">
            <p>
              Utility Assist needs your help to offset it's overhead expenses.
              We rely on on the generosity of donors like you to fund our
              operational costs. Any donation applied to a Need Number that is
              greater than the need will help fund Utility Assist Overhead
              costs.
            </p>
            <div className="col">
              <p>Thank you for including a tip of:</p>
            </div>
            <div className="col">
              <div className="btn-group btn-group-toggle" data-toggle="buttons">
                <button
                  className="btn btn-outline-success"
                  onClick={() => this.props.addTip(this.props.total * 0.1)}
                >
                  10% ({(this.props.total * 0.1).toFixed(2)})
                </button>
                <button
                  className="btn btn-outline-success"
                  onClick={() => this.props.addTip(this.props.total * 0.15)}
                >
                  15% ({(this.props.total * 0.15).toFixed(2)})
                </button>
                <button
                  className="btn btn-outline-success"
                  onClick={() => this.props.addTip(this.props.total * 0.2)}
                >
                  20% ({(this.props.total * 0.2).toFixed(2)})
                </button>
              </div>
              <p>Total Charge: {this.props.tip + this.props.total}</p>
              <label className="font-weight-light">
                <StripeCheckout
                  token={this.onToken}
                  stripeKey="pk_live_d8yJyDLVdgpyUnN62yDWM593"
                  name="Utility Assist"
                  billingAddress={true}
                  description={
                    "Your Donation of  $" +
                    (this.props.stripeAmount / 100).toFixed(2)
                  }
                  currency="USD"
                >
                  <button
                    type="button"
                    className="btn btn-success mr-3"
                    disabled
                  >
                    Checkout
                  </button>
                  (please add tip amount)
                </StripeCheckout>
              </label>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div id="stickyCart">
          <h1>Your Cart</h1>
          <div className="jumbotron" id="cartJumbo">
            <div className="form-row mr-5">
            <div className="col">
            <p className="lead ">Count</p>
            </div>
              <div className="col">
                <p className="lead ">Donation Amount</p>
              </div>
              <div className="col">
                <p className="lead">Need Number</p>
              </div>
            </div>
            {this.props.dollars.map((dollar,i) => (
              <div id="cartRow" className="form-row mr-5">
                <div className="col">
                {i + 1}
                  <button
                    className="btn btn-link"
                    onClick={() => {
                      this.props.removeDonation(
                        dollar.hardship._id,
                        dollar.donation,
                        this.props.index,
                        dollar.filterId
                      );
                    }}
                  >
                    <i className="fas fa-times" />
                  </button>
                </div>

                <div className="col">
                  <p>$ {dollar.donation}</p>
                </div>
                <div className="col">
                  <p>
                    ...
                    {dollar.hardship._id.slice(16, 25)}
                  </p>
                </div>
              </div>
            ))}
            <h2 className="mt-2">Your Donation: ${this.props.total}</h2>
          </div>
          <div id="tipSection" className="jumbotron">
            <p>
              Utility Assist needs your help to offset it's overhead expenses.
              We rely on on the generosity of donors like you to fund our
              opertaional costs. Any donation applied to a Need Number that is
              greater than the need will help fund Utility Assist Overhead
              costs.
            </p>
            <div className="col">
              <p>Thank you for including a tip of:</p>
            </div>
            <div className="col">
              <div className="btn-group btn-group-toggle" data-toggle="buttons">
                <button
                  className="btn btn-outline-success"
                  onClick={() => this.props.addTip(this.props.total * 0.1)}
                >
                  10% ({(this.props.total * 0.1).toFixed(2)})
                </button>
                <button
                  className="btn btn-outline-success"
                  onClick={() => this.props.addTip(this.props.total * 0.15)}
                >
                  15% ({(this.props.total * 0.15).toFixed(2)})
                </button>
                <button
                  className="btn btn-outline-success"
                  onClick={() => this.props.addTip(this.props.total * 0.2)}
                >
                  20% ({(this.props.total * 0.2).toFixed(2)})
                </button>
              </div>
              <p>Total Charge: {this.props.tip + this.props.total}</p>
              <StripeCheckout
                token={this.onToken}
                stripeKey="pk_live_d8yJyDLVdgpyUnN62yDWM593"
                billingAddress={true}
                name="Utility Assist"
                description={
                  "Your Donation of  $" +
                  (this.props.stripeAmount / 100).toFixed(2)
                }
                currency="usd"
              >
                <button type="button" className="btn btn-success">
                  Checkout
                </button>
              </StripeCheckout>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapDispatchToProps = dispatch => {
  return {
    removeDonation: (donationId, amount, index, filter) =>
      dispatch({
        type: actionTypes.REMOVE_DONATION,
        payload: {
          donationId,
          amount,
          index,
          filter
        }
      }),
    addTip: tip =>
      dispatch({
        type: "ADD_TIP",
        payload: {
          tip
        }
      })
  };
};

const mapStateToProps = state => {
  return {
    dollars: state.hardship.selectedDonation,
    total: state.hardship.total,
    index: state.hardship.index,
    stripeAmount: state.hardship.stripeAmount,
    tip: state.hardship.tip,
    basicAmount: state.hardship.basicAmount,
    transactionGroupId: state.hardship.transactionGroupId
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);
