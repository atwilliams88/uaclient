import React, { Component } from 'react'
import './Donate.css'
import { Link } from 'react-router-dom'

export default class Donate extends Component {
  render() {
    return (
      <Link id ="LinkStyle" to="/needs">
        <div id="donateBox" className="justify-content-center bg-light mainSpacing">
        <div className="col-lg-10 offset-lg-1 ">
            <h1 id="donateCta">Donate</h1>
            <p>Families all across the greater St.Louis Area need your help with Utility Payments. By donating through Utility Assist your gift will be tax deductible and will be sent directly to the utility provider.</p>
        </div>
      </div>
      </Link>
    )
  }
}
