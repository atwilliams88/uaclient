import React, {Component} from 'react';
import axios from "axios";

class ContactUs2 extends Component {
    constructor(props) {
        super(props);
        this.state ={
            formSubmitted: false,
            firstName: "",
            lastName:"",
            email:"",
            subject:"",
            question: ""
        }
    }
  handleSubmit(e){
      axios({
          method: "POST",
          url: '/contactUs',
          data: {
              firstName: this.state.firstName,
              lastName: this.state.lastName,
              email: this.state.email,
              topic: this.state.subject,
              description: this.state.question
          }
      }).then((response)=> {
          // console.log(response)
      }).catch((error)=> {
          // console.log(error)
      })
  }
    render() {
      if(this.state.formSubmitted===false) {
          return (
              <div>
                  <h1>Contact Us</h1>
                  <form onSubmit={(e)=>this.handleSubmit(e)}>
                      <input
                          type="text"
                          placeholder="First Name"
                          onChange={(e)=>{this.setState({firstName:e.target.value})}}
                      ></input>
                      <br/>
                      <input
                          type="text"
                          placeholder="Last Name"
                          onChange={(e)=>{this.setState({lastName:e.target.value})}}
                      ></input>
                      <br/>
                      <input
                          type="text"
                          placeholder="Email"
                          onChange={(e)=>{this.setState({email:e.target.value})}}
                      ></input>
                      <br/>
                      <input
                          type="text"
                          placeholder="Subject"
                          onChange={(e)=>{this.setState({subject:e.target.value})}}
                      ></input>
                      <br/>
                      <textarea
                          rows={8}
                          cols={100}
                          type="text"
                          placeholder="Description"
                          onChange={(e)=>{this.setState({question:e.target.value})}}
                      ></textarea>
                      <br/>
                      <button
                          type="submit"
                          onClick={e=>{this.setState({formSubmitted:true})}}
                      >Submit
                      </button>
                  </form>
              </div>
          );
      }else if (this.state.formSubmitted===true){
          return(
              <div>
                  <h2>Thank you</h2>
                  <p> We will be in touch soon.</p>
              </div>
              )
      }
    }
}

export default ContactUs2;