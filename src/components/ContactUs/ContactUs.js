import React from 'react'
import { withFormik, Form, Field } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'

const ContactUs =({
    values,
    handleChange,
    touched,
    errors
}) => (
    <Form className="form-group">
        <h1>Contact Us</h1>
            <div className="col col-lg-2">
                {touched.firstName && errors.firstName && <p className="newAlert">{errors.firstName}</p> }
                <Field 
                type="text"
                name="firstName"
                className="form-control mb-2"
                placeholder="First Name"/>
            </div>
            <div className="col col-lg-2">
                {touched.lastName && errors.lastName  && <p className="newAlert">{errors.lastName}</p> }
                <Field 
                type="text"
                name="lastName"
                className="form-control mb-2"
                placeholder="Last Name"/>
            </div>
            <div className="col col-lg-4">
                {touched.email && errors.email && <p className="newAlert">{errors.email}</p> }
                <Field
                type="email"
                name="email"
                className="form-control"
                placeholder="email address"/>
            </div>
        <br/>
        <div>
            <label> 
                <small className="ml-3">Select Topic</small>
                <Field className="form-control ml-3" component="select"  name="topic">
                    <option value="Donation">About Donations</option>
                    <option value="Collaborate">Working Together</option>
                    <option value="Hardship">Hardship Status</option>
                    <option value="Other">Other</option>
                </Field>
            </label>
        </div>
        <div>
            {touched.question && errors.question && <p className="newAlert">{errors.question}</p> }
            <textarea className="form-control ml-3" rows="4" cols="50" name="question" value={values.question} onChange={handleChange}></textarea>
        </div>
        <button className="btn btn-primary ml-3 mt-3"type="submit">Submit</button>
    </Form>
)

const FormikContactUs = withFormik({
   mapPropsToValues({firstName,lastName,topic,question,email}){
       return {
        firstName:firstName,
        lastName:lastName,
        email:email,
        topic:topic || "Donation",
        question:question
       }
   },
   validationSchema: Yup.object().shape({
        firstName: Yup.string().min(2),
        lastName: Yup.string().min(2),
        email: Yup.string().email().required("Email Is Needed for Response"),
        question: Yup.string().required("Please Enter Your Question Or Comment") 
   }),

   handleSubmit (values){
       // console.log(values)
       axios({
           method: "POST",
           url: '/contactUs',
           data: {
               firstName: values.firstName,
               lastName: values.lastName,
               email: values.email,
               topic: values.topic,
               description: values.question
           }
       }).then((response)=> {
           // console.log(response)
       }).catch((error)=> {
           // console.log(error)
       })
   }
})(ContactUs);

export default FormikContactUs