import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import registerServiceWorker from './registerServiceWorker';
import thunk from 'redux-thunk'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { composeWithDevTools } from 'redux-devtools-extension';
import LogRocket from 'logrocket';

// import Loadable from 'react-loadable';
import 'bootstrap/dist/css/bootstrap.css';
import './pages/index.css'

//reducers Section

import userReducer from './store/reducers/user'
import cartReducer from './store/reducers/cart'
import sidebarReducer from './store/reducers/sidebar'


///Pages Import Section

import Home from './pages/Home'
import Needs from './pages/Needs'
import NewUser from './pages/NewUser'
import User from './pages/User'
import Admin from './pages/Admin'
import SignIn from './components/User/SignIn/SignIn'
import NeedId from './pages/NeedId'
LogRocket.init('wfn3mh/utility-assist');


// Loadable Section
// const Loading = ()=> <div>...loading</div>
// const AsyncHome= Loadable({
//     loader: ()=> import('./pages/Home'),
//     loading: Loading
// });
// const AsyncNeeds= Loadable({
//     loader: ()=> import('./pages/Needs'),
//     loading: Loading
// });
// const AsyncNewUser= Loadable({
//     loader: ()=> import('./pages/NewUser'),
//     loading: Loading
// });
// const AsyncAdmin= Loadable({
//     loader: ()=> import('./pages/Admin'),
//     loading: Loading
// });
// const AsyncSignIn= Loadable({
//     loader: ()=> import('./components/User/SignIn/SignIn'),
//     loading: Loading
// });
// const AsyncUser= Loadable({
//     loader: ()=> import('./pages/User'),
//     loading: Loading
// });


const rootReducer = combineReducers({
    user: userReducer,
    hardship: cartReducer,
    sidebar: sidebarReducer
});
const store = createStore(rootReducer,composeWithDevTools(), applyMiddleware(thunk,LogRocket.reduxMiddleware()));

ReactDOM.render(
<Provider store={store}>
    <Router>
        <Switch>
                {/* pages --- Home */}
            <Route 
            path='/'
            exact
            component={Home}
            />
            {/* pages --- Needs */}
            <Route
            path='/needs'
            exact
            component={Needs}
            />
            {/* pages --- NewUser */}
            <Route 
            path='/account/create'
            exact
            component={NewUser}
            />
             <Route 
            path='/account/login'
            exact
            component={SignIn}
            />
            {/* pages --- User */}
            <Route 
            path='/account/overview'
            exact
            component={User}
            />
            {/* pages --- Admin */}
            <Route 
            path='/admin'
            exact
            component={Admin}
            />
            <Route 
            path='/needs/:id'
            exact
            component={NeedId}
            />
        </Switch>
    </Router>
    {/* <App/> */}
</Provider>, 

document.getElementById('root'));
registerServiceWorker();
